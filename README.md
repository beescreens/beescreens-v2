# BeeScreens - Interactive screens for events.

- [BeeScreens - Interactive screens for events.](#beescreens---interactive-screens-for-events)
  - [Introduction](#introduction)
  - [Run BeeScreens](#run-beescreens)
    - [Prerequisites](#prerequisites)
    - [Prepare the application](#prepare-the-application)
    - [Deploy the application](#deploy-the-application)
  - [BeeScreens development](#beescreens-development)
    - [Files structure](#files-structure)
    - [Tools](#tools)
    - [Testing](#testing)
    - [Contributing](#contributing)
      - [Workflow](#workflow)
      - [Issues](#issues)
      - [Merge requests](#merge-requests)
      - [Unit testing](#unit-testing)
  - [Third-party app development](#third-party-app-development)
  - [How does it work](#how-does-it-work)
    - [Technologies](#technologies)
      - [Backend](#backend)
      - [Frontends](#frontends)
  - [Authors](#authors)
  - [License](#license)

## Introduction

BeeScreens allows to stream applications from one media to any other media supporting a Web Browser (i.e. a phone, computer, etc).

It is a framework allowing any third-party developpers to develop new interactive applications to be streamed over the Internet.

## Run BeeScreens

These instructions will get you a copy of the project up and running on your computer.

### Prerequisites

- [`docker`](https://www.docker.com/) must be installed.
- [`docker-compose`](https://www.docker.com/) must be installed.
- [`git`](https://git-scm.com/) must be installed.

### Prepare the application

```sh
# Clone the repository
git clone https://gitlab.com/beescreens/beescreens.git

# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
mv .env.dist .env
```

### Deploy the application

```sh
# Start the infrastructure
docker-compose up
```

The application is now deployed:

- [localhost:8080](http://localhost:8080): Access the API.
- [localhost:8081](http://localhost:8081): Configure a new display.
- [localhost:8082](http://localhost:8082): Access the application menu.
- ([localhost:8084](http://localhost:8084): Access the administration panel.)

## BeeScreens development

These instructions will get you a copy of the project up and running on your
computer for development and testing purposes.

### Files structure

The project is splitted in two main parts: `src/` and `test/`.

`src/` contains all the sources of the project:

- [`src/admin/`](https://gitlab.com/beescreens/beescreens/tree/master/src/admin)
- [`src/api/`](https://gitlab.com/beescreens/beescreens/tree/master/src/api)
- [`src/apps/`](https://gitlab.com/beescreens/beescreens/tree/master/src/apps)
- [`src/db/`](https://gitlab.com/beescreens/beescreens/tree/master/src/db)
- [`src/display/`](https://gitlab.com/beescreens/beescreens/tree/master/src/display)
- [`src/play/`](https://gitlab.com/beescreens/beescreens/tree/master/src/play)

`test/` contains all the unit testing for the different aspects of the application:

- [`test/admin/`](https://gitlab.com/beescreens/beescreens/tree/master/test/admin)
- [`test/api/`](https://gitlab.com/beescreens/beescreens/tree/master/test/api)
- [`test/apps/`](https://gitlab.com/beescreens/beescreens/tree/master/test/apps)
- [`test/db/`](https://gitlab.com/beescreens/beescreens/tree/master/test/db)
- [`test/display/`](https://gitlab.com/beescreens/beescreens/tree/master/test/display)
- [`test/play/`](https://gitlab.com/beescreens/beescreens/tree/master/test/play)

See this as a mirror: when you implement a new feature in `src/`, you have the related unit testing in `test/`.

Every directory has its own `README` with instructions on how to launch, how to test, etc.
Have a look there to know more about every part !

### Tools

We recommend you to use the following tools to develop on BeeScreens but it's really up to you.

- [Visual Studio Code](https://code.visualstudio.com/) with the following extensions installed:
  - [Docker](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker)
  - [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv)
  - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
  - [Swagger Viewer](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer)
  - [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)

### Testing

We recommend you to test the application before starting to develop by following the *[Run BeeScreens](#run-beescreens)* instructions.

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
mv .env.dist .env

# The infrastructure starts and is tested
docker-compose -f docker-compose-testing.yml up --build <the part of the application you want to test>
```

Replace the *`<the part of the application you want to test>`* with the part of the application you would like to test:

- `admin-test`: Test the administration panel.
- `api-test`: Test the API.
- `display-test`: The display configuration menu.
- `play-test`: The play menu.

### Contributing

You want to contribute to BeeScreens ? Great ! Here is some help to get you started to contribute to the project.

#### Workflow

Over the weeks, we have come to a workflow that we find efficient and easy to use. You are free to work on your side by forking the project but if you want to contribute in the same repository as use, please consider using this workflow.

1. Create a new issue describing the feature you would like to implement (do not forget the labels :)).
2. Just after thant, create a new merge request by pressing the button "Create merge request". This will create a new WIP branch associated to the merge request.
3. Pull and switch to that new branch. Implement the new feature and push anytime you want.
4. When your new feature is ready to be merged with `master`, go back to the merge request and press "Resolve WIP status". This will check that you can merge with `master`.
5. If your feature can be merged to `master`, check the checkbox "Remove source branch". We do not want to keep old branchs in our repository.
6. You can then merge with master. This will automatically close the issue, delete the branch and your new feature is now on `master` ! Congrats !

**Tips**
- Iterate with small issues at the time. The smaller feature you do, the better as many things can change in the repository by the time you finish your work and the sooner other people can get your work, the better for them as well !
- `master` branch is protected. No one can push directly to `master`. This is a way we guarantee things on the `master` branch are stable and should be working.

#### Issues

Issues are the primary way we use for communication. Issues can be about anything: new features, bugs as well as discussions, questions and other things.

We use labels to quickly identify issues and their content. Please use labels as much and as accurate possible to describe the issue, it is very helpful !

You can find the complete list of labels and their meaning [here](https://gitlab.com/beescreens/beescreens/labels).

Feel free to open a issue if you want to improve and discuss about something !

#### Merge requests

Feel free to submit merge requests ! Please do not forget the right labels as well as a comprehensive description about your new feature.

#### Unit testing

Unit testing is a good and automated way to know that we have not broke something by implementing our new features. Consider improving unit testings in the `test/` directory.

## Third-party app development

As our framework will "only" allow to deploy, view, render and moderate applications, the
developers are more than welcome to develop fun and interactive ways to play with screens.

Have a look at [`src/apps/`](https://gitlab.com/beescreens/beescreens/tree/master/apps) for further documentation.

## How does it work
TODO

### Technologies

#### Backend

- [NodeJS](https://nodejs.org) as the server's core technology.
- [TypeScript](https://www.typescriptlang.org/) as the main language.
- [Express](https://expressjs.com/) as the web framework.
- [Swagger](https://swagger.io/) to specify and generate the API.
- [PeerJS](https://peerjs.com/) to abstract the WebRTC implementation.
- [socket.io](https://socket.io/) for real-time communication.
- [MongoDB](https://www.mongodb.com/) as the database.
- [Mongoose](https://mongoosejs.com/) as the Object-Document Mapping (ODM).
- [TSLint](https://palantir.github.io/tslint/) as the linter for the project.
- [Mocha](https://mochajs.org/) as the testing framework.
- [Chai](https://www.chaijs.com/) as the assertion library.
- Many other Node's libraries...

#### Frontends

The client sides use the following technologies only to display the available applications:

- [Vue.js](https://vuejs.org/) as the the front-end framework.
- [bootstrap-vue](https://bootstrap-vue.js.org/) as the front-end CSS framework.
- [vue-fontawesome](https://github.com/FortAwesome/vue-fontawesome) as the web's most popular icon set and toolkit.

## Authors

This project was made at [HEIG-VD](https://heig-vd.ch/), Switzerland and was initially
brought to you by:

* Ludovic Delafontaine
* Vincent Guidoux
* Guillaume Hochet
* Xavier Vaz Afonso

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md)
file for details.
