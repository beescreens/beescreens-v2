# BeeScreens - `test/api`

This test the following part of the application:

- [`src/api`](https://gitlab.com/beescreens/beescreens/tree/master/src/api)

## Launch

### Locally

```sh
# Move to the cloned directory
cd beescreens/test/api/src/

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the API unit testing
npm start
```

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
mv .env.dist .env

# Start the API unit testing
docker-compose -f docker-compose-testing.yml up --build api-test
```
