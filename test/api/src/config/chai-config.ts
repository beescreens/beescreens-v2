import chai from 'chai';

const dirty = require('dirty-chai');

const { expect } = chai;
const should = chai.should();

chai.use(dirty);

export {
    expect,
    should,
};
