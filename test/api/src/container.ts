const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

import * as awilix from 'awilix';

import {
    expect,
    should,
} from './config/chai-config';

const {
    asValue, createContainer, InjectionMode,
} = awilix;

const container = createContainer({
    injectionMode: InjectionMode.PROXY,
});

// Register parameters
container.register({
    expect: asValue(expect),
    should: asValue(should),

    protocol: asValue(process.env.PROTOCOL),
    apiHostname: asValue(process.env.API_HOSTNAME),
    apiPort: asValue(process.env.API_PORT),

    url: asValue(`${process.env.PROTOCOL}//${process.env.API_HOSTNAME}:${process.env.API_PORT}`),

    adminUsername: asValue(process.env.ADMIN_USERNAME),
    adminPassword: asValue(process.env.ADMIN_PASSWORD),

    acceptNewApplications: asValue(process.env.ACCEPT_NEW_APPLICATIONS),
    acceptKnownApplications: asValue(process.env.ACCEPT_KNOWN_APPLICATIONS),
    applicationsPassword: asValue(process.env.APPLICATIONS_PASSWORD),

    acceptNewDisplayers: asValue(process.env.ACCEPT_NEW_DISPLAYERS),
    acceptKnownDisplayers: asValue(process.env.ACCEPT_KNOWN_DISPLAYERS),
    displayersPassword: asValue(process.env.DISPLAYERS_PASSWORD),

    acceptPlayers: asValue(process.env.ACCEPT_PLAYERS),
    playersPassword: asValue(process.env.PLAYERS_PASSWORD),
});

// Export container
export {
    container,
};
