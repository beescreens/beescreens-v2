import { Response } from 'express';

export class Response201 {
    static send(
        body: object,
        res: Response,
    ) {
        res.status(201);
        res.json(body);
        res.end();
    }
}
