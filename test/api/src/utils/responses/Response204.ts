import { Response } from 'express';

export class Response204 {
    static send(
        res: Response,
    ) {
        res.status(204);
        res.end();
    }
}
