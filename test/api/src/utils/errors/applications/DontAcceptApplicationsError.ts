export class DontAcceptApplicationsError extends Error {
    constructor() {
        super('The server doesn\'t accept new applications.');

        Error.captureStackTrace(this, this.constructor);
    }
}
