export class OnlyAcceptKnownDisplayersError extends Error {
    constructor() {
        super('The server only accept known displayers.');

        Error.captureStackTrace(this, this.constructor);
    }
}
