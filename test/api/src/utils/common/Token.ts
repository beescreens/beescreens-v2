import { EntityType } from './EntityType';
import { Role } from './Role';

export class Token {
    constructor(
        public role: EntityType | Role,
        public id: string,
    ) {
        // Empty
    }
}
