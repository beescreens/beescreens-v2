export enum Role {
    ADMIN = 'admin',
    MODERATOR = 'moderator',
    READ_ONLY = 'read-only',
}
