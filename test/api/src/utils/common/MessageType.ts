export enum MessageType {
    SESSION_READY = 'session-ready',
    SESSION_GO = 'session-go',
    SESSION_END = 'session-end',

    APPLICATION_READY = 'application-ready',
    DISPLAYER_READY = 'displayer-ready',
    PLAYER_READY = 'player-ready',

    PLAYER_DATA = 'player-data',
    APPLICATION_DATA = 'application-data',

    DISCONNECT = 'disconnect',

    QUEUE = 'queue',

    MESSAGE = 'message',
}
