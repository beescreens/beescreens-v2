import { Author } from '../utils/common/Author';
import { Status } from '../utils/common/Status';

export class ApplicationDTO {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public logo: string,
        public homepage: string,
        public documentation: string,
        public version: number,
        public authors: Array<Author>,
        public status: Status,
    ) {
        // Empty
    }
}
