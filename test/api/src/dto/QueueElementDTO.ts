export class QueueElementDTO {
    constructor(
        public playerId: string,
        public applicationId: string,
        public position: number,
    ) {
        // Empty
    }
}
