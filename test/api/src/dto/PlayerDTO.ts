import { Status } from '../utils/common/Status';

export class PlayerDTO {
    constructor(
        public id: string,
        public status: Status,
    ) {
        // Empty
    }
}
