export class JwtDTO {
    constructor(
        public jwt: string,
        public timestamp: Date,
    ) {
        // Empty
    }
}
