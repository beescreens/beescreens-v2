import { Status } from '../utils/common/Status';

export class DisplayerDTO {
    constructor(
        public name: string,
        public location: string,
        public width: number,
        public height: number,
        public status: Status,
    ) {
        // Empty
    }
}
