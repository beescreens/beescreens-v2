import { Role } from '../utils/common/Role';
import { Status } from '../utils/common/Status';

export class UserDTO {
    constructor(
        public username: string,
        public role: Role,
        public status: Status,
    ) {
        // Empty
    }
}
