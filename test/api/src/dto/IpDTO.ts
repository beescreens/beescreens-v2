export class IpDTO {
    constructor(
        public ip: string,
        public timestamp: Date,
    ) {
        // Empty
    }
}
