export class SessionDTO {
    constructor(
        public id: string,
        public playerId: string,
        public applicationId: string,
        public displayerId: string,
    ) {
        // Empty
    }
}
