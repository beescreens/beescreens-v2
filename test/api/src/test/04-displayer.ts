import axios from 'axios';

import { container } from '../container';
import { DisplayerDTO } from '../dto/DisplayerDTO';
import { DisplayerPost } from '../models/displayers/DisplayerPost';
import { CredentialsPost } from '../models/login-logout/CredentialsPost';
import { OptionPatch } from '../models/options/OptionPatch';
import { Status } from '../utils/common/Status';
import { DisplayerPatch } from '../models/displayers/DisplayerPatch';

describe('Displayer', () => {
    const adminUsername: string = container.cradle.adminUsername;
    const adminPassword: string = container.cradle.adminPassword;
    const expect: Chai.ExpectStatic = container.cradle.expect;
    const url: string = container.cradle.url;

    const acceptNewDisplayersDefaultValue: string = container.cradle.acceptNewDisplayers;
    const acceptKnownDisplayersDefaultValue: string = container.cradle.acceptKnownDisplayers;
    const displayersPasswordDefaultValue: string = container.cradle.displayersPassword;

    const client = axios.create({
        baseURL: url,
        timeout: 2000,
    });

    const adminUser = new CredentialsPost(
        adminUsername,
        adminPassword,
    );

    const defaultAcceptNewDisplayers = new OptionPatch(
        'accept_new_displayers',
        acceptNewDisplayersDefaultValue,
    );

    const defaultAcceptKnownDisplayers = new OptionPatch(
        'accept_known_displayers',
        acceptKnownDisplayersDefaultValue,
    );

    const defaultDisplayersPassword = new OptionPatch(
        'displayers_password',
        displayersPasswordDefaultValue,
    );

    const displayer1 = new DisplayerPost(
        'g01-01',
        'G01-01',
        'Principal tour',
        1024,
        768,
    );

    const displayer2 = new DisplayerPost(
        'g02-01',
        'G02-01',
        'Principal tour',
        1024,
        768,
    );

    const displayer3 = new DisplayerPost(
        'g03-01',
        'G03-01',
        'Principal tour',
        1024,
        768,
    );

    const displayer4 = new DisplayerPost(
        'g04-01',
        'G04-01',
        'Principal tour',
        1024,
        768,
        'WRONG_PASSWORD',
    );

    const displayer5 = new DisplayerPost(
        'g05-01',
        'G05-01',
        'Principal tour',
        1024,
        768,
        'RIGHT_PASSWORD',
    );

    const displayer6 = new DisplayerPost(
        'g06-01',
        'G06-01',
        'Principal tour',
        1024,
        768,
    );

    const patchedDisplayer5 = new DisplayerPatch(
        displayer5.id,
        Status.INACTIVE,
    );

    let adminToken: string;
    let displayer1Token: string;
    let displayer2Token: string;
    let displayer5Token: string;

    before(async () => {
        const res = await client.post('/login', adminUser, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        adminToken = data.jwt;
    });

    after(async () => {
        await client.patch(
            `/options/${defaultAcceptNewDisplayers.optionId}`,
            defaultAcceptNewDisplayers,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.patch(
            `/options/${defaultAcceptKnownDisplayers.optionId}`,
            defaultAcceptKnownDisplayers,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.patch(
            `/options/${defaultDisplayersPassword.optionId}`,
            defaultDisplayersPassword,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.delete(`/displayers/${displayer2.id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        await client.delete(`/displayers/${displayer5.id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        /*
        await client.post('/logout', null, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });
        */
    });

    describe(`New displayers: true | Known displayers: true | Password: ''`, () => {
        const acceptNewDisplayers = new OptionPatch(
            defaultAcceptNewDisplayers.optionId,
            'true',
        );

        const acceptKnownDisplayers = new OptionPatch(
            defaultAcceptKnownDisplayers.optionId,
            'true',
        );

        const displayersPassword = new OptionPatch(
            defaultDisplayersPassword.optionId,
            '',
        );

        it('As a displayer, I can register as a new displayer', async () => {
            await client.patch(
                `/options/${acceptNewDisplayers.optionId}`,
                acceptNewDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownDisplayers.optionId}`,
                acceptKnownDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${displayersPassword.optionId}`,
                displayersPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            const res = await client.post('/displayers', displayer1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            displayer1Token = data.jwt;
        });

        it('As a displayer, I can come back as a known displayer', async () => {
            const res = await client.post('/displayers', displayer1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });
    });

    describe(`New displayers: false | Known displayers: true | Password: ''`, () => {
        const acceptNewDisplayers = new OptionPatch(
            defaultAcceptNewDisplayers.optionId,
            'false',
        );

        const acceptKnownDisplayers = new OptionPatch(
            defaultAcceptKnownDisplayers.optionId,
            'true',
        );

        const displayersPassword = new OptionPatch(
            defaultDisplayersPassword.optionId,
            '',
        );

        it('As a displayer, I cannot register as a new displayer...', async () => {
            await client.patch(
                `/options/${acceptNewDisplayers.optionId}`,
                acceptNewDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownDisplayers.optionId}`,
                acceptKnownDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${displayersPassword.optionId}`,
                displayersPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/displayers', displayer2, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('...but can come back as a known displayer', async () => {
            const res = await client.post('/displayers', displayer1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });
    });

    describe(`New displayers: true | Known displayers: false | Password: ''`, () => {
        const acceptNewDisplayers = new OptionPatch(
            defaultAcceptNewDisplayers.optionId,
            'true',
        );

        const acceptKnownDisplayers = new OptionPatch(
            defaultAcceptKnownDisplayers.optionId,
            'false',
        );

        const displayersPassword = new OptionPatch(
            defaultDisplayersPassword.optionId,
            '',
        );

        it('As a displayer, I cannot come back as a known displayer...', async () => {
            await client.patch(
                `/options/${acceptNewDisplayers.optionId}`,
                acceptNewDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownDisplayers.optionId}`,
                acceptKnownDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${displayersPassword.optionId}`,
                displayersPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/displayers', displayer1, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('...but can register as a new displayer', async () => {
            const res = await client.post('/displayers', displayer2, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            displayer2Token = data.jwt;
        });
    });

    describe(`New displayers: true | Known displayers: true | Password: 'RIGHT_PASSWORD'`, () => {
        const acceptNewDisplayers = new OptionPatch(
            defaultAcceptNewDisplayers.optionId,
            'true',
        );

        const acceptKnownDisplayers = new OptionPatch(
            defaultAcceptKnownDisplayers.optionId,
            'true',
        );

        const displayersPassword = new OptionPatch(
            defaultDisplayersPassword.optionId,
            'RIGHT_PASSWORD',
        );

        it('As a displayer, I cannot register without a required password', async () => {
            await client.patch(
                `/options/${acceptNewDisplayers.optionId}`,
                acceptNewDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownDisplayers.optionId}`,
                acceptKnownDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${displayersPassword.optionId}`,
                displayersPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/displayers', displayer3, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a displayer, I cannot register as a displayer with a wrong password', async () => {
            try {
                await client.post('/displayers', displayer4, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a displayer, I can register as a displayer with the right password', async () => {
            const res = await client.post('/displayers', displayer5, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            displayer5Token = data.jwt;
        });

        it('As a displayer, I can come back as a known displayer', async () => {
            const res = await client.post('/displayers', displayer5, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });
    });

    describe('New displayers: false | Known displayers: false | Password: N/A', () => {
        const acceptNewDisplayers = new OptionPatch(
            defaultAcceptNewDisplayers.optionId,
            'false',
        );

        const acceptKnownDisplayers = new OptionPatch(
            defaultAcceptKnownDisplayers.optionId,
            'false',
        );

        it('As a displayer, I cannot register as a new displayer', async () => {
            await client.patch(
                `/options/${acceptNewDisplayers.optionId}`,
                acceptNewDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownDisplayers.optionId}`,
                acceptKnownDisplayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/displayers', displayer6, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a displayer, I cannot register as a known displayer', async () => {
            try {
                await client.post('/displayers', displayer5, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });
    });

    describe('As an admin...', () => {
        it('...I can get a displayer', async () => {
            const res = await client.get(`/displayers/${displayer1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(200);

            const { data } = res;

            expect(data).to.have.property('id');
            expect(data).to.have.property('name');
            expect(data).to.have.property('location');
            expect(data).to.have.property('width');
            expect(data).to.have.property('height');
            expect(data).to.have.property('status');

            const {
                id,
                name,
                location,
                width,
                height,
                status,
            } = data;

            id.should.equal(displayer1.id);
            name.should.equal(displayer1.name);
            location.should.equal(displayer1.location);
            width.should.equal(displayer1.width);
            height.should.equal(displayer1.height);
            status.should.equal(Status.ACTIVE);
        });

        it('...I can get all displayers', async () => {
            const res = await client.get('/displayers', {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(200);

            const { data } = res;

            // data.should.have.lengthOf(2);

            data.forEach((displayer: DisplayerDTO) => {
                expect(displayer).to.have.property('id');
                expect(displayer).to.have.property('name');
                expect(displayer).to.have.property('location');
                expect(displayer).to.have.property('width');
                expect(displayer).to.have.property('height');
                expect(displayer).to.have.property('status');
            });
        });

        it('...I can delete a displayer', async () => {
            const res = await client.delete(`/displayers/${displayer1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });

        it('...I can update a displayer', async () => {
            let res;

            res = await client.patch(`/displayers/${displayer5.id}`, patchedDisplayer5, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);

            res = await client.get(`/displayers/${displayer5.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            const { data } = res;

            const {
                id,
                name,
                location,
                width,
                height,
                status,
            } = data;

            id.should.equal(patchedDisplayer5.displayerId);
            name.should.equal(displayer5.name);
            location.should.equal(displayer5.location);
            width.should.equal(displayer5.width);
            height.should.equal(displayer5.height);
            status.should.equal(patchedDisplayer5.status);
        });
    });
});
