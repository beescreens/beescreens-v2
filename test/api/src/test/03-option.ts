import axios from 'axios';

import { container } from '../container';
import { OptionDTO } from '../dto/OptionDTO';
import { CredentialsPost } from '../models/login-logout/CredentialsPost';
import { OptionPatch } from '../models/options/OptionPatch';

describe('Option', () => {
    const adminUsername: string = container.cradle.adminUsername;
    const adminPassword: string = container.cradle.adminPassword;
    const expect: Chai.ExpectStatic = container.cradle.expect;
    const url: string = container.cradle.url;

    const acceptNewDisplayers: string = container.cradle.acceptNewDisplayers;

    const client = axios.create({
        baseURL: url,
        timeout: 500,
    });

    const adminUser = new CredentialsPost(
        adminUsername,
        adminPassword,
    );

    let token: string;

    const existigOption = new OptionPatch(
        'accept_new_displayers',
        acceptNewDisplayers,
    );

    const patchedOption = new OptionPatch(
        'accept_new_displayers',
        'false',
    );

    const unknownOption = new OptionPatch(
        'unknown_option',
        null,
    );

    before(async () => {
        const res = await client.post('/login', adminUser, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        token = data.jwt;
    });

    after(async () => {
        await client.patch(`/options/${existigOption.optionId}`, existigOption, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        /*
        await client.post('/logout', null, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });
        */
    });

    it(`Can get the option '${existigOption.optionId}'`, async () => {
        const res = await client.get(`/options/${existigOption.optionId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(200);

        const { data } = res;

        expect(data).to.have.property('id');
        expect(data).to.have.property('name');
        expect(data).to.have.property('description');
        expect(data).to.have.property('value');

        const {
            value,
        } = data;

        value.should.equal(existigOption.value);
    });

    it(`Can change the value of the option '${existigOption.optionId}'`, async () => {
        let res;

        res = await client.patch(`/options/${existigOption.optionId}`, patchedOption, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(204);

        res = await client.get(`/options/${existigOption.optionId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        const {
            id,
            value,
        } = data;

        id.should.equal(patchedOption.optionId);
        value.should.equal(patchedOption.value);
    });

    it('Can get all options', async () => {
        const res = await client.get('/options', {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(200);

        const { data } = res;

        data.should.have.lengthOf(8);

        data.forEach((option: OptionDTO) => {
            expect(option).to.have.property('id');
            expect(option).to.have.property('name');
            expect(option).to.have.property('description');
            expect(option).to.have.property('value');
        });
    });

    it(`An unknown option shoud return 404 ('${unknownOption.optionId}')`, async () => {
        try {
            await client.get(`/options/${unknownOption.optionId}`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            });
        } catch({ response }) {
            response.status.should.equal(404);
        }
    });
});
