import axios from 'axios';

import { container } from '../container';
import { PlayerDTO } from '../dto/PlayerDTO';
import { PlayPost } from '../models/play/PlayPost';
import { CredentialsPost } from '../models/login-logout/CredentialsPost';
import { OptionPatch } from '../models/options/OptionPatch';
import { Status } from '../utils/common/Status';
import { PlayerPatch } from '../models/players/PlayerPatch';
import { ApplicationPost } from '../models/applications/ApplicationPost';
import { Author } from '../utils/common/Author';

describe('Player', () => {
    const adminUsername: string = container.cradle.adminUsername;
    const adminPassword: string = container.cradle.adminPassword;
    const expect: Chai.ExpectStatic = container.cradle.expect;
    const url: string = container.cradle.url;

    const acceptPlayersDefaultValue: string = container.cradle.acceptPlayers;
    const playersPasswordDefaultValue: string = container.cradle.playersPassword;

    const client = axios.create({
        baseURL: url,
        timeout: 2000,
    });

    const adminUser = new CredentialsPost(
        adminUsername,
        adminPassword,
    );

    const defaultAcceptPlayers = new OptionPatch(
        'accept_players',
        acceptPlayersDefaultValue,
    );

    const defaultPlayersPassword = new OptionPatch(
        'players_password',
        playersPasswordDefaultValue,
    );

    const application1 = new ApplicationPost(
        'app1',
        'Application 1',
        'First application',
        'http://localhost:8083/logo.png',
        'http://localhost:8083',
        'http://localhost:8083/docs',
        1.0,
        [
            new Author(
                'App1 dev',
                'dev@app1.com',
                'dev1.com',
            ),
        ],
    );

    const application2 = new ApplicationPost(
        'app2',
        'Application 2',
        'Second application',
        'http://localhost:8084/logo.png',
        'http://localhost:8084',
        'http://localhost:8084/docs',
        1.0,
        [
            new Author(
                'App2 dev',
                'dev@app2.com',
                'dev2.com',
            ),
        ],
    );

    const player1 = new PlayPost(
        application1.id,
    );

    const player2 = new PlayPost(
        application2.id,
        'WRONG_PASSWORD',
    );

    const player3 = new PlayPost(
        application2.id,
        'RIGHT_PASSWORD',
    );

    const player4 = new PlayPost(
        application2.id,
    );

    const player5 = new PlayPost(
        'unknown_app',
    );

    let adminToken: string;

    let player1Id: string;
    let player1Token: string;

    let player3Id: string;
    let player3Token: string;

    /*
    const patchedPlayer5 = new PlayerPatch(
        player5.id,
        Status.INACTIVE,
    );
    */

    before(async () => {
        const res = await client.post('/login', adminUser, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        adminToken = data.jwt;

        await client.post('/apps', application1, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        await client.post('/apps', application2, {
            headers: {
                'Content-Type': 'application/json',
            },
        });
    });

    after(async () => {
        await client.patch(
            `/options/${defaultAcceptPlayers.optionId}`,
            defaultAcceptPlayers,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.patch(
            `/options/${defaultPlayersPassword.optionId}`,
            defaultPlayersPassword,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.delete(`/apps/${application1.id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        await client.delete(`/apps/${application2.id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        /*
        await client.delete(`/players/${player3Id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        /*
        await client.post('/logout', null, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });
        */
    });

    describe(`Players: true | Password: ''`, () => {
        const acceptNewPlayers = new OptionPatch(
            defaultAcceptPlayers.optionId,
            'true',
        );

        const playersPassword = new OptionPatch(
            defaultPlayersPassword.optionId,
            '',
        );

        it('As a player, I can register as a new player', async () => {
            await client.patch(
                `/options/${acceptNewPlayers.optionId}`,
                acceptNewPlayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${playersPassword.optionId}`,
                playersPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            const res2 = await client.get(`/apps/${application1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            const data2 = res2.data;
            console.log(data2);

            const res = await client.post('/play', player1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            player1Token = data.jwt;
        });

        it('As a player, I cannot register if application is unknown', async () => {
            try {
                await client.post('/play', player5, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(404);
            }
        });
    });

    describe(`Players: true | Password: 'RIGHT_PASSWORD'`, () => {
        const acceptNewPlayers = new OptionPatch(
            defaultAcceptPlayers.optionId,
            'true',
        );

        const playersPassword = new OptionPatch(
            defaultPlayersPassword.optionId,
            'RIGHT_PASSWORD',
        );

        it('As a player, I cannot register without a required password', async () => {
            await client.patch(
                `/options/${acceptNewPlayers.optionId}`,
                acceptNewPlayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${playersPassword.optionId}`,
                playersPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/play', player3, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a player, I cannot register as a player with a wrong password', async () => {
            try {
                await client.post('/play', player2, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a player, I can register as a player with the right password', async () => {
            const res = await client.post('/play', player3, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            player3Token = data.jwt;
        });
    });

    describe('Players: false | Password: N/A', () => {
        const acceptNewPlayers = new OptionPatch(
            defaultAcceptPlayers.optionId,
            'false',
        );

        it('As a player, I cannot register as a new player', async () => {
            await client.patch(
                `/options/${acceptNewPlayers.optionId}`,
                acceptNewPlayers,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/play', player4, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });
    });

    describe('As an admin...', () => {
        /*
        it('...I can get a player', async () => {
            const res = await client.get(`/players/${player1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(200);

            const { data } = res;

            expect(data).to.have.property('id');
            expect(data).to.have.property('name');
            expect(data).to.have.property('description');
            expect(data).to.have.property('logo');
            expect(data).to.have.property('homepage');
            expect(data).to.have.property('documentation');
            expect(data).to.have.property('version');
            expect(data).to.have.property('authors');
            expect(data).to.have.property('status');

            const {
                id,
                name,
                description,
                logo,
                homepage,
                documentation,
                version,
                authors,
                status,
            } = data;

            id.should.equal(player1.id);
            name.should.equal(player1.name);
            description.should.equal(player1.description);
            logo.should.equal(player1.logo);
            homepage.should.equal(player1.homepage);
            documentation.should.equal(player1.documentation);
            version.should.equal(player1.version);
            authors.should.have.lengthOf(1);

            for (let index = 0; index < authors.length; index++) {
                const author = authors[index];
                const defaultAuthor = player1.authors[index];

                author.name.should.equal(defaultAuthor.name);
                author.email.should.equal(defaultAuthor.email);
                author.homepage.should.equal(defaultAuthor.homepage);
            }

            status.should.equal(Status.ACTIVE);
        });
        */

        it('...I can get all players', async () => {
            const res = await client.get('/players', {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(200);

            const { data } = res;

            // data.should.have.lengthOf(2);

            data.forEach((player: PlayerDTO) => {
                expect(player).to.have.property('id');
                expect(player).to.have.property('status');
            });
        });

        /*
        it('...I can delete a player', async () => {
            const res = await client.delete(`/players/${player1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });

        it('...I can update a player', async () => {
            let res;

            res = await client.patch(`/players/${player5.id}`, patchedPlayer5, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);

            res = await client.get(`/players/${player5.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            const { data } = res;

            const {
                id,
                name,
                description,
                logo,
                homepage,
                documentation,
                version,
                authors,
                status,
            } = data;

            id.should.equal(patchedPlayer5.playerId);
            name.should.equal(player5.name);
            description.should.equal(player5.description);
            logo.should.equal(player5.logo);
            homepage.should.equal(player5.homepage);
            documentation.should.equal(player5.documentation);
            version.should.equal(player5.version);
            authors.should.have.lengthOf(1);

            for (let index = 0; index < authors.length; index++) {
                const author = authors[index];
                const defaultAuthor = player5.authors[index];

                author.name.should.equal(defaultAuthor.name);
                author.email.should.equal(defaultAuthor.email);
                author.homepage.should.equal(defaultAuthor.homepage);
            }

            status.should.equal(patchedPlayer5.status);
        });
        */
    });
});
