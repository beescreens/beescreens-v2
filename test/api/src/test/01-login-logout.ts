import axios from 'axios';

import { container } from '../container';
import { CredentialsPost } from '../models/login-logout/CredentialsPost';

describe('Login/Logout', () => {
    const adminUsername: string = container.cradle.adminUsername;
    const adminPassword: string = container.cradle.adminPassword;
    const url: string = container.cradle.url;

    const client = axios.create({
        baseURL: url,
        timeout: 500,
    });

    const adminUser = new CredentialsPost(
        adminUsername,
        adminPassword,
    );

    const unknownUser = new CredentialsPost(
        'unknown',
        'unknown',
    );

    const wrongPassword = new CredentialsPost(
        adminUsername,
        'wrong password',
    );

    let token: string;

    it('Can login with an existing user', async () => {
        const res = await client.post('/login', adminUser, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(200);

        const { data } = res;

        data.jwt.should.exist();
        token = data.jwt;
    });

    it('Cannot login with an unknown user', async () => {
        try {
            await client.post('/login', unknownUser, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });
        } catch({ response }) {
            response.status.should.equal(401);
        }
    });

    it('Cannot login with a wrong password', async () => {
        try {
            await client.post('/login', wrongPassword, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });
        } catch({ response }) {
            response.status.should.equal(401);
        }
    });

    /*
    it('Can logout', async () => {
        const res = await client.post('/logout', null, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(204);
    });
    */
});
