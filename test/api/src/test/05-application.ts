import axios from 'axios';

import { container } from '../container';
import { ApplicationDTO } from '../dto/ApplicationDTO';
import { ApplicationPost } from '../models/applications/ApplicationPost';
import { CredentialsPost } from '../models/login-logout/CredentialsPost';
import { OptionPatch } from '../models/options/OptionPatch';
import { Status } from '../utils/common/Status';
import { ApplicationPatch } from '../models/applications/ApplicationPatch';
import { Author } from '../utils/common/Author';

describe('Application', () => {
    const adminUsername: string = container.cradle.adminUsername;
    const adminPassword: string = container.cradle.adminPassword;
    const expect: Chai.ExpectStatic = container.cradle.expect;
    const url: string = container.cradle.url;

    const acceptNewApplicationsDefaultValue: string = container.cradle.acceptNewApplications;
    const acceptKnownApplicationsDefaultValue: string = container.cradle.acceptKnownApplications;
    const applicationsPasswordDefaultValue: string = container.cradle.applicationsPassword;

    const client = axios.create({
        baseURL: url,
        timeout: 2000,
    });

    const adminUser = new CredentialsPost(
        adminUsername,
        adminPassword,
    );

    const defaultAcceptNewApplications = new OptionPatch(
        'accept_new_applications',
        acceptNewApplicationsDefaultValue,
    );

    const defaultAcceptKnownApplications = new OptionPatch(
        'accept_known_applications',
        acceptKnownApplicationsDefaultValue,
    );

    const defaultApplicationsPassword = new OptionPatch(
        'applications_password',
        applicationsPasswordDefaultValue,
    );

    const application1 = new ApplicationPost(
        'app1',
        'Application 1',
        'First application',
        'http://localhost:8083/logo.png',
        'http://localhost:8083',
        'http://localhost:8083/docs',
        1.0,
        [
            new Author(
                'App1 dev',
                'dev@app1.com',
                'dev1.com',
            ),
        ],
    );

    const application2 = new ApplicationPost(
        'app2',
        'Application 2',
        'Second application',
        'http://localhost:8084/logo.png',
        'http://localhost:8084',
        'http://localhost:8084/docs',
        1.0,
        [
            new Author(
                'App2 dev',
                'dev@app2.com',
                'dev2.com',
            ),
        ],
    );

    const application3 = new ApplicationPost(
        'app3',
        'Application 3',
        'Thrid application',
        'http://localhost:8085/logo.png',
        'http://localhost:8085',
        'http://localhost:8085/docs',
        1.0,
        [
            new Author(
                'App3 dev',
                'dev@app3.com',
                'dev3.com',
            ),
        ],
    );

    const application4 = new ApplicationPost(
        'app4',
        'Application 4',
        'Fourth application',
        'http://localhost:8086/logo.png',
        'http://localhost:8086',
        'http://localhost:8086/docs',
        1.0,
        [
            new Author(
                'App4 dev',
                'dev@app4.com',
                'dev4.com',
            ),
        ],
        'WRONG_PASSWORD',
    );

    const application5 = new ApplicationPost(
        'app5',
        'Application 5',
        'Fifth application',
        'http://localhost:8087/logo.png',
        'http://localhost:8087',
        'http://localhost:8087/docs',
        1.0,
        [
            new Author(
                'App7 dev',
                'dev@app7.com',
                'dev7.com',
            ),
        ],
        'RIGHT_PASSWORD',
    );

    const application6 = new ApplicationPost(
        'app6',
        'Application 6',
        'Sixth application',
        'http://localhost:8088/logo.png',
        'http://localhost:8088',
        'http://localhost:8088/docs',
        1.0,
        [
            new Author(
                'App8 dev',
                'dev@app8.com',
                'dev8.com',
            ),
        ],
    );

    const patchedApplication5 = new ApplicationPatch(
        application5.id,
        Status.INACTIVE,
    );

    let adminToken: string;
    let application1Token: string;
    let application2Token: string;
    let application5Token: string;

    before(async () => {
        const res = await client.post('/login', adminUser, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        adminToken = data.jwt;
    });

    after(async () => {
        await client.patch(
            `/options/${defaultAcceptNewApplications.optionId}`,
            defaultAcceptNewApplications,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.patch(
            `/options/${defaultAcceptKnownApplications.optionId}`,
            defaultAcceptKnownApplications,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.patch(
            `/options/${defaultApplicationsPassword.optionId}`,
            defaultApplicationsPassword,
            {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        await client.delete(`/apps/${application2.id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        await client.delete(`/apps/${application5.id}`, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });

        /*
        await client.post('/logout', null, {
            headers: {
                Authorization: `Bearer ${adminToken}`,
                'Content-Type': 'application/json',
            },
        });
        */
    });

    describe(`New applications: true | Known applications: true | Password: ''`, () => {
        const acceptNewApplications = new OptionPatch(
            defaultAcceptNewApplications.optionId,
            'true',
        );

        const acceptKnownApplications = new OptionPatch(
            defaultAcceptKnownApplications.optionId,
            'true',
        );

        const applicationsPassword = new OptionPatch(
            defaultApplicationsPassword.optionId,
            '',
        );

        it('As a application, I can register as a new application', async () => {
            await client.patch(
                `/options/${acceptNewApplications.optionId}`,
                acceptNewApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownApplications.optionId}`,
                acceptKnownApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${applicationsPassword.optionId}`,
                applicationsPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            const res = await client.post('/apps', application1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            application1Token = data.jwt;
        });

        it('As a application, I can come back as a known application', async () => {
            const res = await client.post('/apps', application1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });
    });

    describe(`New applications: false | Known applications: true | Password: ''`, () => {
        const acceptNewApplications = new OptionPatch(
            defaultAcceptNewApplications.optionId,
            'false',
        );

        const acceptKnownApplications = new OptionPatch(
            defaultAcceptKnownApplications.optionId,
            'true',
        );

        const applicationsPassword = new OptionPatch(
            defaultApplicationsPassword.optionId,
            '',
        );

        it('As a application, I cannot register as a new application...', async () => {
            await client.patch(
                `/options/${acceptNewApplications.optionId}`,
                acceptNewApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownApplications.optionId}`,
                acceptKnownApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${applicationsPassword.optionId}`,
                applicationsPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/apps', application2, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('...but can come back as a known application', async () => {
            const res = await client.post('/apps', application1, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });
    });

    describe(`New applications: true | Known applications: false | Password: ''`, () => {
        const acceptNewApplications = new OptionPatch(
            defaultAcceptNewApplications.optionId,
            'true',
        );

        const acceptKnownApplications = new OptionPatch(
            defaultAcceptKnownApplications.optionId,
            'false',
        );

        const applicationsPassword = new OptionPatch(
            defaultApplicationsPassword.optionId,
            '',
        );

        it('As a application, I cannot come back as a known application...', async () => {
            await client.patch(
                `/options/${acceptNewApplications.optionId}`,
                acceptNewApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownApplications.optionId}`,
                acceptKnownApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${applicationsPassword.optionId}`,
                applicationsPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/apps', application1, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('...but can register as a new application', async () => {
            const res = await client.post('/apps', application2, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            application2Token = data.jwt;
        });
    });

    describe(`New applications: true | Known applications: true | Password: 'RIGHT_PASSWORD'`, () => {
        const acceptNewApplications = new OptionPatch(
            defaultAcceptNewApplications.optionId,
            'true',
        );

        const acceptKnownApplications = new OptionPatch(
            defaultAcceptKnownApplications.optionId,
            'true',
        );

        const applicationsPassword = new OptionPatch(
            defaultApplicationsPassword.optionId,
            'RIGHT_PASSWORD',
        );

        it('As a application, I cannot register without a required password', async () => {
            await client.patch(
                `/options/${acceptNewApplications.optionId}`,
                acceptNewApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownApplications.optionId}`,
                acceptKnownApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${applicationsPassword.optionId}`,
                applicationsPassword,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/apps', application3, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a application, I cannot register as a application with a wrong password', async () => {
            try {
                await client.post('/apps', application4, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a application, I can register as a application with the right password', async () => {
            const res = await client.post('/apps', application5, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(201);

            const { data } = res;

            data.jwt.should.exist();

            application5Token = data.jwt;
        });

        it('As a application, I can come back as a known application', async () => {
            const res = await client.post('/apps', application5, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });
    });

    describe('New applications: false | Known applications: false | Password: N/A', () => {
        const acceptNewApplications = new OptionPatch(
            defaultAcceptNewApplications.optionId,
            'false',
        );

        const acceptKnownApplications = new OptionPatch(
            defaultAcceptKnownApplications.optionId,
            'false',
        );

        it('As a application, I cannot register as a new application', async () => {
            await client.patch(
                `/options/${acceptNewApplications.optionId}`,
                acceptNewApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            await client.patch(
                `/options/${acceptKnownApplications.optionId}`,
                acceptKnownApplications,
                {
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                },
            );

            try {
                await client.post('/apps', application6, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });

        it('As a application, I cannot register as a known application', async () => {
            try {
                await client.post('/apps', application5, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            } catch({ response }) {
                response.status.should.equal(403);
            }
        });
    });

    describe('As an admin...', () => {
        it('...I can get a application', async () => {
            const res = await client.get(`/apps/${application1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(200);

            const { data } = res;

            expect(data).to.have.property('id');
            expect(data).to.have.property('name');
            expect(data).to.have.property('description');
            expect(data).to.have.property('logo');
            expect(data).to.have.property('homepage');
            expect(data).to.have.property('documentation');
            expect(data).to.have.property('version');
            expect(data).to.have.property('authors');
            expect(data).to.have.property('status');

            const {
                id,
                name,
                description,
                logo,
                homepage,
                documentation,
                version,
                authors,
                status,
            } = data;

            id.should.equal(application1.id);
            name.should.equal(application1.name);
            description.should.equal(application1.description);
            logo.should.equal(application1.logo);
            homepage.should.equal(application1.homepage);
            documentation.should.equal(application1.documentation);
            version.should.equal(application1.version);
            authors.should.have.lengthOf(1);

            for (let index = 0; index < authors.length; index++) {
                const author = authors[index];
                const defaultAuthor = application1.authors[index];

                author.name.should.equal(defaultAuthor.name);
                author.email.should.equal(defaultAuthor.email);
                author.homepage.should.equal(defaultAuthor.homepage);
            }

            status.should.equal(Status.ACTIVE);
        });

        it('...I can get all applications', async () => {
            const res = await client.get('/apps', {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(200);

            const { data } = res;

            // data.should.have.lengthOf(2);

            data.forEach((application: ApplicationDTO) => {
                expect(application).to.have.property('id');
                expect(application).to.have.property('name');
                expect(application).to.have.property('description');
                expect(application).to.have.property('logo');
                expect(application).to.have.property('homepage');
                expect(application).to.have.property('documentation');
                expect(application).to.have.property('version');
                expect(application).to.have.property('authors');
                expect(application).to.have.property('status');
            });
        });

        it('...I can delete a application', async () => {
            const res = await client.delete(`/apps/${application1.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);
        });

        it('...I can update a application', async () => {
            let res;

            res = await client.patch(`/apps/${application5.id}`, patchedApplication5, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            res.status.should.equal(204);

            res = await client.get(`/apps/${application5.id}`, {
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
            });

            const { data } = res;

            const {
                id,
                name,
                description,
                logo,
                homepage,
                documentation,
                version,
                authors,
                status,
            } = data;

            id.should.equal(patchedApplication5.applicationId);
            name.should.equal(application5.name);
            description.should.equal(application5.description);
            logo.should.equal(application5.logo);
            homepage.should.equal(application5.homepage);
            documentation.should.equal(application5.documentation);
            version.should.equal(application5.version);
            authors.should.have.lengthOf(1);

            for (let index = 0; index < authors.length; index++) {
                const author = authors[index];
                const defaultAuthor = application5.authors[index];

                author.name.should.equal(defaultAuthor.name);
                author.email.should.equal(defaultAuthor.email);
                author.homepage.should.equal(defaultAuthor.homepage);
            }

            status.should.equal(patchedApplication5.status);
        });
    });
});
