import axios from 'axios';

import { container } from '../container';
import { CredentialsPost } from '../models/login-logout/CredentialsPost';
import { UserPost } from '../models/users/UserPost';
import { Role } from '../utils/common/Role';
import { Status } from '../utils/common/Status';
import { UserDTO } from '../dto/UserDTO';
import { UserPatch } from '../models/users/UserPatch';

describe('User', () => {
    const adminUsername: string = container.cradle.adminUsername;
    const adminPassword: string = container.cradle.adminPassword;
    const expect: Chai.ExpectStatic = container.cradle.expect;
    const url: string = container.cradle.url;

    const client = axios.create({
        baseURL: url,
        timeout: 500,
    });

    const adminUser = new CredentialsPost(
        adminUsername,
        adminPassword,
    );

    const newUser = new UserPost(
        'username',
        'password',
        Role.ADMIN,
    );

    const patchedUser = new UserPatch(
        newUser.username,
        'newPassword',
        Role.MODERATOR,
        Status.BANNED,
    );

    let token: string;

    before(async () => {
        const res = await client.post('/login', adminUser, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        token = data.jwt;
    });

    after(async () => {
        /*
        await client.post('/logout', null, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });
        */
    });

    it('Can create a new user', async () => {
        const res = await client.post('/users', newUser, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(204);
    });

    it('Cannot create the same new user', async () => {
        try {
            await client.post('/users', newUser, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            });
        } catch({ response }) {
            response.status.should.equal(409);
        }
    });

    it('Can get the created user', async () => {
        const res = await client.get(`/users/${newUser.username}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(200);

        const { data } = res;

        expect(data).to.have.property('username');
        expect(data).to.have.property('role');
        expect(data).to.have.property('status');

        const {
            username,
            role,
            status,
        } = data;

        username.should.equal(newUser.username);
        role.should.equal(newUser.role);
        status.should.equal(Status.ACTIVE);
    });

    it('Can get all the users', async () => {
        const res = await client.get('/users', {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(200);

        const { data } = res;

        data.should.have.lengthOf(2);

        data.forEach((user: UserDTO) => {
            expect(user).to.have.property('username');
            expect(user).to.have.property('role');
            expect(user).to.have.property('status');
        });
    });

    it('Can update the created user', async () => {
        let res;

        res = await client.patch(`/users/${newUser.username}`, patchedUser, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(204);

        res = await client.get(`/users/${newUser.username}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        const { data } = res;

        const {
            username,
            role,
            status,
        } = data;

        username.should.equal(patchedUser.username);
        role.should.equal(patchedUser.role);
        status.should.equal(patchedUser.status);
    });

    it('Can delete the created user', async () => {
        const res = await client.delete(`/users/${newUser.username}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        res.status.should.equal(204);
    });

    it('Cannot get the deleted user', async () => {
        try {
            await client.get(`/users/${newUser.username}`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            });
        } catch ({ response }) {
            response.status.should.equal(404);
        }
    });
});
