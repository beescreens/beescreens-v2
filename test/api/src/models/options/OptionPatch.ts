export class OptionPatch {
    constructor(
        public optionId: string,
        public value: string,
    ) {
        // Empty
    }
}
