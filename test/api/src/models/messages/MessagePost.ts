import { Level } from '../../utils/common/Level';

export class MessagePost {
    constructor(
        public entitiyId: string,
        public title: string,
        public content: string,
        public level: Level,
    ) {
        // Empty
    }
}
