import { Status } from '../../utils/common/Status';

export class PlayerPatch {
    constructor(
        public playerId: string,
        public status: Status,
    ) {
        // Empty
    }
}
