import { Author } from '../../utils/common/Author';

export class ApplicationPost {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public logo: string,
        public homepage: string,
        public documentation: string,
        public version: number,
        public authors: Array<Author>,
        public password?: string,
    ) {
        // Empty
    }
}
