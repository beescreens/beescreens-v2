import { Status } from '../../utils/common/Status';

export class ApplicationPatch {
    constructor(
        public applicationId: string,
        public status: Status,
    ) {
        // Empty
    }
}
