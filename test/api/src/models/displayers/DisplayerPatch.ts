import { Status } from '../../utils/common/Status';

export class DisplayerPatch {
    constructor(
        public displayerId: string,
        public status: Status,
    ) {
        // Empty
    }
}
