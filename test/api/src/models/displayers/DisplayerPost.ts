export class DisplayerPost {
    constructor(
        public id: string,
        public name: string,
        public location: string,
        public width: number,
        public height: number,
        public password?: string,
    ) {
        // Empty
    }
}
