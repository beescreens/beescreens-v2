import { Status } from '../../utils/common/Status';
import { Role } from '../../utils/common/Role';

export class UserPatch {
    constructor(
        public username: string,
        public password: string,
        public role: Role,
        public status: Status,
    ) {
        // Empty
    }
}
