import { Role } from '../../utils/common/Role';

export class UserPost {
    constructor(
        public username: string,
        public password: string,
        public role: Role,
    ) {
        // Empty
    }
}
