#!/usr/bin/env bash
######################################################
# Name:             run.sh
# Author:           BeeScreens
# Creation:         29.06.2015
# Description:      Start unit testing when ressource is available.
######################################################

./wait-for-it.sh --host=${API_HOSTNAME} --port=${API_PORT}

exitStatus=$?

if [[ $exitStatus == 0 ]]; then
    npm start

    exitStatus=$?
fi

exit $exitStatus
