/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

import displayerApi from './displayer-api';
import peer from './peerServer';

Vue.use(Vuex);

const vuexLocal = new VuexPersist({
    storage: window.localStorage,
});

export default new Vuex.Store({
    state: {
        id: null,
        name: null,
        location: null,
        password: null,
        width: null,
        height: null,
        jwt: null,
    },
    mutations: {
        saveConfig(state, config) {
            const {
                id,
                name,
                password,
                location,
                width,
                height,
            } = config;

            state.id = id;
            state.name = name;
            state.location = location;
            state.password = password;
            state.width = width;
            state.height = height;
        },
        saveJwt(state, data) {
            const {
                jwt,
            } = data;

            state.jwt = jwt;
        },
        clear(state) {
            state.id = null;
            state.name = null;
            state.location = null;
            state.password = null;
            state.width = null;
            state.height = null;
            state.jwt = null;
        },
    },
    getters: {
        hasJwt(state) {
            return state.jwt != null;
        },
    },
    actions: {
        getJwt(context) {
            return new Promise((resolve, reject) => {
                const url = `${process.env.VUE_APP_PROTOCOL}//${process.env.VUE_APP_API_HOSTNAME}:${process.env.VUE_APP_API_PORT}/displayers`;

                fetch(url, {
                    method: 'POST',
                    headers: { 'content-type': 'application/json' },
                    body: JSON.stringify({
                        id: context.state.id,
                        name: context.state.name,
                        location: context.state.location,
                        width: JSON.parse(context.state.width),
                        height: JSON.parse(context.state.height),
                        password: context.state.password,
                    }),
                })
                    .then((res) => {
                        if (res.status === 201) {
                            res.json()
                                .then((data) => {
                                    context.commit('saveJwt', data);
                                    resolve();
                                });
                        } else {
                            resolve();
                        }
                    })
                    .catch((err) => {
                        reject(err);
                    });
            });
        },
        connectSocket(context) {
            const url = `${process.env.VUE_APP_API_HOSTNAME}:${process.env.VUE_APP_API_PORT}/displayers`;

            return displayerApi.connect(url, context.state.jwt);
        },
        connectPeer(context) {
            return peer.connect(
                context.state.id,
                process.env.VUE_APP_API_HOSTNAME,
                process.env.VUE_APP_API_PORT,
            );
        },
    },
    plugins: [vuexLocal.plugin],
});
