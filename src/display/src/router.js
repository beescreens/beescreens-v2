import Vue from 'vue';
import Router from 'vue-router';

import Config from './views/Config.vue';
import Displayer from './views/Displayer.vue';

import store from './store';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'displayer',
            component: Displayer,
        },
        {
            path: '/config',
            name: 'config',
            component: Config,
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (to.name !== 'config' && !store.getters.hasJwt) {
        next({ name: 'config' });
    } else {
        next();
    }
});

export default router;
