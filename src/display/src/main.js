import Vue from 'vue';
import Toasted from 'vue-toasted';

import App from './App.vue';
import displayerApi from './displayer-api';
import router from './router';
import store from './store';

Vue.prototype.displayerApi = displayerApi;

Vue.use(Toasted, {
    position: 'center-center',
    duration: 4000,
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
