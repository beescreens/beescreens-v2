
import PeerJs from 'peerjs';

class Peer {
    constructor() {
        this.peer = null;
    }

    connect(id, hostname, port) {
        return new Promise((resolve, reject) => {
            this.peer = new PeerJs(id, {
                host: hostname,
                port,
                path: '/peers',
                config: {
                    iceServers: [
                        { urls: 'stun:stun.l.google.com:19302' },
                        { urls: 'stun:stun1.l.google.com:19302' },
                        {
                            urls: 'turn:numb.viagenie.ca',
                            username: 'webrtc@live.com',
                            credential: 'muazkh',
                        },
                        {
                            urls: 'turn:192.158.29.39:3478?transport=udp',
                            username: '28224511:1379330808',
                            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                        },
                        {
                            urls: 'turn:192.158.29.39:3478?transport=tcp',
                            username: '28224511:1379330808',
                            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                        },
                    ],
                },
            });

            this.peer.on('open', () => {
                this.connected = true;
                resolve();
            });

            this.peer.on('disconnected', () => {
                this.disconnect();
            });

            this.peer.on('error', (error) => {
                this.connected = false;

                this.disconnect();

                reject(error);
            });
        });
    }

    onCall(fn) {
        this.peer.on('call', fn);
    }

    disconnect() {
        if (this.connected) {
            this.peer.destroy();
        }

        this.connected = false;
    }
}

export default new Peer();
