/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        apps: [],
    },
    getters: {
        apps(state) {
            return state.apps;
        },
        serverUrl() {
            const url = `${process.env.VUE_APP_PROTOCOL}//${process.env.VUE_APP_API_HOSTNAME}:${process.env.VUE_APP_API_PORT}`;

            return url;
        },
    },
    mutations: {
        pushApp(state, app) {
            state.apps.push(app);
        },
    },
});
