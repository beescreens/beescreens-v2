import Vue from 'vue';
import Router from 'vue-router';

import Menu from './views/Menu.vue';

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'menu',
        component: Menu,
    },
];

const router = new Router({
    mode: 'history',
    routes,
});

export default router;
