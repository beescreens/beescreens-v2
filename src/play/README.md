# BeeScreens - `src/play/`

This part of the application depends on:

- [`src/api/`](https://gitlab.com/beescreens/beescreens/tree/master/src/api)

And can be tested with:

- [`test/play/`](https://gitlab.com/beescreens/beescreens/tree/master/test/play)

## Launch

### Locally

```sh
# Move to the cloned directory
cd beescreens/src/play/

# Copy and edit the environment variables
cp .env.dist .env

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the UI
npm run serve
```

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
cp .env.dist .env

# Start the UI
docker-compose up --build play
```
