import VueRouter from 'vue-router';
import routes from './routes';

// configure router
const router = new VueRouter({
    mode: 'history',
    routes, // short for routes: routes
    linkExactActiveClass: 'active',
    scrollBehavior: (to) => {
        if (to.hash) {
            return { selector: to.hash };
        }
        return { x: 0, y: 0 };
    },
});

/*
router.beforeEach((to, from, next) => {
    // Public pages
    const publicPages = [
        '/login',
    ];

    const isPublicPage = publicPages.includes(to.path);

    const loggedIn = localStorage.getItem('user');

    if (!loggedIn && !isPublicPage) {
        next('/login');
    } else {
        next();
    }
});
*/

export default router;
