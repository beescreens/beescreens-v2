import DashboardLayout from '@/layout/dashboard/DashboardLayout.vue';
// GeneralViews
import NotFound from '@/pages/NotFoundPage';

import Login from '@/pages/Login.vue';
import Dashboard from '@/pages/Dashboard.vue';
import Users from '@/pages/Users/Users.vue';
import Displayers from '@/pages/Displayers/Displayers.vue';
import Applications from '@/pages/Applications/Applications.vue';
import Players from '@/pages/Players/Players.vue';
import Options from '@/pages/Options/Options.vue';
import Sessions from '@/pages/Sessions/Sessions.vue';
import Queue from '@/pages/Queue/Queue.vue';

const routes = [
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/',
        component: DashboardLayout,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: Dashboard,
            },
            {
                path: 'users',
                name: 'users',
                component: Users,
            },
            {
                path: 'displayers',
                name: 'displayers',
                component: Displayers,
            },
            {
                path: 'applications',
                name: 'applications',
                component: Applications,
            },
            {
                path: 'players',
                name: 'players',
                component: Players,
            },
            {
                path: 'options',
                name: 'options',
                component: Options,
            },
            {
                path: 'sessions',
                name: 'sessions',
                component: Sessions,
            },
            {
                path: 'queue',
                name: 'queue',
                component: Queue,
            },
        ],
    },
    { path: '*', component: NotFound },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};* */

export default routes;
