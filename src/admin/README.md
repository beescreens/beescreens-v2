# BeeScreens - `src/admin/`

This part of the application depends on:

- [`src/api/`](https://gitlab.com/beescreens/beescreens/tree/master/src/api)

And can be tested with:

- [`test/admin/`](https://gitlab.com/beescreens/beescreens/tree/master/test/admin)

## Launch

### Locally

```sh
# Move to the cloned directory
cd beescreens/src/admin/

# Copy and edit the environment variables
cp .env.dist .env

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the UI
npm run serve
```

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
cp .env.dist .env

# Start the UI
docker-compose up --build admin
```

## Other

This program is based on the following template made by Creative Tim (https://www.creative-tim.com/) under a MIT licence. More can be found here: [Vue-Black Dashboard](https://demos.creative-tim.com/vue-black-dashboard).
