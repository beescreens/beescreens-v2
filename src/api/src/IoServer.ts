import debug from 'debug';
import Io from 'socket.io';

import { ApplicationService } from './services/ApplicationService';
import { DisplayerService } from './services/DisplayerService';
import { EntityType } from './utils/common/EntityType';
import { JwtService } from './services/JwtService';
import { PlayerModel } from './models/PlayerModel';
import { PlayService } from './services/PlayService';
import { PlayerService } from './services/PlayerService';
import { QueueService } from './services/QueueService';
import { RestServer } from './RestServer';
import { SessionService } from './services/SessionService';
import { Status } from './utils/common/Status';

const ioAuth = require('socketio-auth');

const log = debug('beescreens:io-server');

export class IoServer {
    /**
     * The constructor.
     */
    constructor(
        public applicationService: ApplicationService,
        public displayerService: DisplayerService,
        public jwtService: JwtService,
        public playService: PlayService,
        public playerService: PlayerService,
        public queueService: QueueService,
        public sessionService: SessionService,
        public restServer: RestServer,
    ) {
        // Empty
    }

    /**
     * Start the server.
     */
    start() {
        const {
            applicationService,
            displayerService,
            jwtService,
            playService,
            playerService,
            queueService,
            restServer,
            sessionService,
        } = this;

        return new Promise<void>((resolve) => {
            const io = Io(restServer.instance);

            const applicationsEndpoint = io.of('/applications');
            const displayersEndpoint = io.of('/displayers');
            const playersEndpoint = io.of('/players');

            const authenticateApplication = (
                socket: Io.Socket,
                data: any,
                callback: Function,
            ) => {
                const { jwt } = data;

                jwtService.verify(jwt)
                    .then((decodedToken) => {
                        const isAuthorized = decodedToken.role != null
                            && decodedToken.role === EntityType.APPLICATION;

                        if (isAuthorized) {
                            applicationService.getApplication(decodedToken.id)
                                .then((application) => {
                                    log(`Application '${application.id}' connected.`);

                                    application.socket = socket;

                                    application.onDisconnect(() => {
                                        log(`Application '${application.id}' disconnected.`);
                                        applicationService.updateApplication(
                                            application.id,
                                            Status.INACTIVE,
                                        );
                                    });

                                    callback(null, true);
                                })
                                .catch(() => {
                                    callback(null, false);
                                });
                        } else {
                            callback(null, false);
                        }
                    })
                    .catch(() => {
                        log('Application not authorized to access /applications');
                        callback(null, false);
                    });
            };

            const authenticateDisplayer = (
                socket: Io.Socket,
                data: any,
                callback: Function,
            ) => {
                const { jwt } = data;

                jwtService.verify(jwt)
                    .then((decodedToken) => {
                        const isAuthorized = decodedToken.role != null
                            && decodedToken.role === EntityType.DISPLAYER;

                        if (isAuthorized) {
                            // This is wrong, ID can vary from the real name.
                            displayerService.getDisplayer(decodedToken.id)
                                .then((displayer) => {
                                    log(`Displayer '${displayer.id}' connected.`);

                                    displayer.socket = socket;

                                    displayer.onDisconnect(() => {
                                        log(`Displayer '${displayer.id}' disconnected.`);
                                        displayerService.updateDisplayer(
                                            displayer.id,
                                            Status.ACTIVE,
                                        );
                                    });

                                    return displayerService.updateDisplayer(
                                        displayer.id,
                                        Status.INACTIVE,
                                    );
                                })
                                .then(() => {
                                    callback(null, true);
                                })
                                .catch(() => {
                                    callback(null, false);
                                });
                        } else {
                            callback(null, false);
                        }
                    })
                    .catch(() => {
                        log('Displayer not authorized to access /displayers');
                        callback(null, false);
                    });
            };

            const authenticatePlayer = (
                socket: Io.Socket,
                data: any,
                callback: Function,
            ) => {
                const { jwt } = data;

                jwtService.verify(jwt)
                    .then((decodedToken) => {
                        const isAuthorized = decodedToken.role != null
                            && decodedToken.role === EntityType.PLAYER;

                        if (isAuthorized) {
                            let tempPlayer: PlayerModel;

                            playerService
                                // This is wrong, ID can vary from the real name.
                                .getPlayer(decodedToken.id)
                                .then((player) => {
                                    log(`Player '${player.id}' connected.`);

                                    tempPlayer = player;

                                    player.socket = socket;

                                    return playService.joinQueue(player.id);
                                })
                                .then((queueElement) => {
                                    const {
                                        position,
                                        playerId,
                                        applicationId,
                                    } = queueElement;

                                    tempPlayer.onDisconnect(() => {
                                        log(`Player '${playerId}' disconnected.`);
                                        queueService.deleteQueueElement(position)
                                            .catch(() => {
                                                // Nothing
                                            });
                                        playerService.deletePlayer(playerId)
                                            .catch(() => {
                                                // Nothing
                                            });
                                    });

                                    log(`Player '${playerId}' joined the `
                                        + `queue at position ${position} to `
                                        + `play '${applicationId}'.`);

                                    sessionService.askToStartSession()
                                        .catch(() => {
                                            // Nothing
                                        });

                                    callback(null, true);
                                })
                                .catch(() => {
                                    callback(null, false);
                                });
                        } else {
                            callback(null, false);
                        }
                    }).catch(() => {
                        log('Player not authorized to access /players');
                        callback(null, false);
                    });
            };

            const authorizedApplication = () => {
                log('An application has been authorized.');
            };

            const authorizedDisplayer = () => {
                log('A displayer has been authorized.');

                sessionService.askToStartSession()
                    .catch(() => {
                        // Do nothing
                    });
            };

            const authorizedPlayer = () => {
                log('A player has been authorized.');

                sessionService.askToStartSession()
                    .catch(() => {
                        // Do nothing
                    });
            };

            const applicationDisconnects = () => {
                log('An application has disconnected.');
            };

            const displayerDisconnects = () => {
                log('A displayer has disconnected.');
            };

            const playerDisconnects = () => {
                log('A player has disconnected.');
            };

            ioAuth(applicationsEndpoint, {
                authenticate: authenticateApplication,
                postAuthenticate: authorizedApplication,
                disconnect: applicationDisconnects,
                timeout: 1000,
            });

            ioAuth(displayersEndpoint, {
                authenticate: authenticateDisplayer,
                postAuthenticate: authorizedDisplayer,
                disconnect: displayerDisconnects,
                timeout: 1000,
            });

            ioAuth(playersEndpoint, {
                authenticate: authenticatePlayer,
                postAuthenticate: authorizedPlayer,
                disconnect: playerDisconnects,
                timeout: 1000,
            });

            log('Ready for connections.');
            resolve();
        });
    }

    /**
     * Stop the server.
     */
    stop() {
        return new Promise<void>((resolve) => {
            log('Server gracefully stopped.');
            resolve();
        });
    }
}
