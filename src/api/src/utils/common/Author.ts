export class Author {
    constructor(
        public name: string,
        public email: string,
        public homepage: string,
    ) {
        // Empty
    }
}
