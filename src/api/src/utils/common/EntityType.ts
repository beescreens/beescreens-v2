export enum EntityType {
    APPLICATION = 'application',
    DISPLAYER = 'displayer',
    PLAYER = 'player',
}
