export enum Level {
    INFO = 'info',
    WARNING = 'warning',
    ERROR = 'error',
}
