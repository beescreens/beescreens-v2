export interface IPlayer {
    emitSessionReady(
        applicationId: string,
        displayerId: string,
        displayerWidth: number,
        displayerHeight: number,
    ): Promise<void>;
    emitSessionGo(applicationId: string, displayerId: string): Promise<void>;
    emitSessionEnd(applicationId: string, displayerId: string): Promise<void>;

    emitApplicationData(data: object): Promise<void>;
    emitQueue(queuePosition: number, queueTotal: number): Promise<void>;

    onPlayerReady(callback: any): Promise<void>;
    onPlayerData(callback: any): Promise<void>;
}
