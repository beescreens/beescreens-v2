export interface IApplication {
    emitSessionReady(
        displayerId: string,
        displayerWidth: number,
        displayerHeight: number,
        playerId: string,
    ): Promise<void>;

    emitSessionGo(
        displayerId: string,
        playerId: string,
    ): Promise<void>;

    emitSessionEnd(
        displayerId: string,
        playerId: string,
    ): Promise<void>;

    emitPlayerData(
        playerId: string,
        data: object,
    ): Promise<void>;

    onApplicationReady(callback: any): Promise<void>;
    onApplicationData(callback: any): Promise<void>;
}
