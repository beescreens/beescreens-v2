import { Level } from './Level';

export interface IEntity {
    emitMessage(title: string, content: string, level: Level): Promise<void>;

    onSessionEnd(callback: any): Promise<void>;
    onDisconnect(callback: any): Promise<void>;
}
