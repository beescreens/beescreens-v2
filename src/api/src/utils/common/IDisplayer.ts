export interface IDisplayer {
    emitSessionReady(applicationId: string, playerId: string): Promise<void>;
    emitSessionGo(applicationId: string, playerId: string): Promise<void>;
    emitSessionEnd(applicationId: string, playerId: string): Promise<void>;

    onDisplayerReady(callback: any): Promise<void>;
}
