export class PasswordIsIncorrectError extends Error {
    constructor() {
        super('The provided password is incorrect.');

        Error.captureStackTrace(this, this.constructor);
    }
}
