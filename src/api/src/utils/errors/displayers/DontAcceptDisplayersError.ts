export class DontAcceptDisplayersError extends Error {
    constructor() {
        super('The server doesn\'t accept new or known displayers.');

        Error.captureStackTrace(this, this.constructor);
    }
}
