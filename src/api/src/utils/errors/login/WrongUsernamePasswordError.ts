export class WrongUsernamePasswordError extends Error {
    constructor() {
        super('The username/password are incorrect.');

        Error.captureStackTrace(this, this.constructor);
    }
}
