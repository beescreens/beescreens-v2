export class OnlyAcceptKnownApplicationsError extends Error {
    constructor() {
        super('The server only accept known applications.');

        Error.captureStackTrace(this, this.constructor);
    }
}
