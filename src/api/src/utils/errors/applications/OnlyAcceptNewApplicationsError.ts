export class OnlyAcceptNewApplicationsError extends Error {
    constructor() {
        super('The server only accept new applications.');

        Error.captureStackTrace(this, this.constructor);
    }
}
