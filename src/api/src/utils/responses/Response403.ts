import { Response } from 'express';

export class Response403 {
    static send(
        res: Response,
    ) {
        res.status(403);
        res.end();
    }
}
