import { Response } from 'express';

export class Response200 {
    static send(
        body: object,
        res: Response,
    ) {
        res.status(200);
        res.json(body);
        res.end();
    }
}
