import { Response } from 'express';

export class Response404 {
    static send(
        res: Response,
    ) {
        res.status(404);
        res.end();
    }
}
