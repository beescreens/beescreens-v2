import { Response } from 'express';

export class Response409 {
    static send(
        res: Response,
    ) {
        res.status(409);
        res.end();
    }
}
