import { Response } from 'express';

export class Response401 {
    static send(
        body: object,
        res: Response,
    ) {
        res.status(401);
        res.json(body);
        res.end();
    }
}

