export class JwtModel {
    constructor(
        public jwt: string,
        public timestamp: Date = new Date(),
    ) {
        // Empty
    }
}
