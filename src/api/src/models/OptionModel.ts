export class OptionModel {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public value: any,
    ) {
        // Empty
    }
}
