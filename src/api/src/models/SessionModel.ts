import uniqid from 'uniqid';

import { PlayerModel } from './PlayerModel';
import { ApplicationModel } from './ApplicationModel';
import { DisplayerModel } from './DisplayerModel';

export class SessionModel {
    public id: string;

    constructor(
        public player: PlayerModel,
        public application: ApplicationModel,
        public displayer: DisplayerModel,
    ) {
        this.id = uniqid();
    }
}
