export class IpModel {
    constructor(
        public ip: string,
        public timestamp: Date = new Date(),
    ) {
        // Empty
    }
}
