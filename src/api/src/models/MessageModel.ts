import { Level } from '../utils/common/Level';

export class MessageModel {
    constructor(
        public title: string,
        public content: string,
        public level: Level,
    ) {
        // Empty
    }
}
