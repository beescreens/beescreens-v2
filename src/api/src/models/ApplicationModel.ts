import { Author } from '../utils/common/Author';
import { IApplication } from '../utils/common/IApplication';
import { IEntity } from '../utils/common/IEntity';
import { Level } from '../utils/common/Level';
import { MessageType } from '../utils/common/MessageType';
import { Status } from '../utils/common/Status';

export class ApplicationModel implements IApplication, IEntity {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public logo: string,
        public homepage: string,
        public documentation: string,
        public version: number,
        public authors: Array<Author>,
        public status: Status = Status.ACTIVE,
        public socket?: SocketIO.Socket,
    ) {
        // Nothing more
    }

    emitMessage(
        title: string,
        content: string,
        level: Level,
    ): Promise<void> {
        const data = {
            title,
            content,
            level,
        };

        return this.emit(MessageType.MESSAGE, data);
    }

    emitSessionReady(
        displayerId: string,
        displayerWidth: number,
        displayerHeight: number,
        playerId: string,
    ) {
        const data = {
            displayerId,
            displayerWidth,
            displayerHeight,
            playerId,
        };

        return this.emit(MessageType.SESSION_READY, data);
    }

    emitSessionGo(
        displayerId: string,
        playerId: string,
    ) {
        const data = {
            displayerId,
            playerId,
        };

        return this.emit(MessageType.SESSION_GO, data);
    }

    emitSessionEnd(
        displayerId: string,
        playerId: string,
    ) {
        const data = {
            displayerId,
            playerId,
        };

        return this.emit(MessageType.SESSION_END, data);
    }

    emitPlayerData(
        playerId: string,
        data: object,
    ) {
        return this.emit(MessageType.PLAYER_DATA, {
            playerId,
            data,
        });
    }

    onApplicationReady(callback: any) {
        return this.on(MessageType.APPLICATION_READY, callback);
    }

    onApplicationData(callback: any) {
        return this.on(MessageType.APPLICATION_DATA, callback);
    }

    onSessionEnd(callback: any): Promise<void> {
        return this.on(MessageType.SESSION_END, callback);
    }

    onDisconnect(callback: any): Promise<void> {
        return this.on(MessageType.DISCONNECT, callback);
    }

    private emit(
        message: MessageType,
        data: any,
    ) {
        const { socket } = this;

        return new Promise<void>((resolve, reject) => {
            if (socket != null) {
                socket.emit(message, data);
                resolve();
            } else {
                reject();
            }
        });
    }

    private on(
        message: MessageType,
        callback: any,
    ) {
        const { socket } = this;

        return new Promise<void>((resolve, reject) => {
            if (socket != null) {
                // socket.removeAllListeners(message);
                socket.on(message, callback);
                resolve();
            } else {
                reject();
            }
        });
    }
}
