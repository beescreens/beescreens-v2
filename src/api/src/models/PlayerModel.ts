import { IPlayer } from '../utils/common/IPlayer';
import { Status } from '../utils/common/Status';
import { IEntity } from '../utils/common/IEntity';
import { MessageType } from '../utils/common/MessageType';
import { Level } from '../utils/common/Level';

export class PlayerModel implements IEntity, IPlayer {
    constructor(
        public id: string,
        public status: Status,
        public socket?: SocketIO.Socket,
    ) {
        // Empty
    }

    emitSessionGo(applicationId: string, displayerId: string): Promise<void> {
        const data = {
            applicationId,
            displayerId,
        };

        return this.emit(MessageType.SESSION_GO, data);
    }

    emitSessionReady(
        applicationId: string,
        displayerId: string,
        displayerWidth: number,
        displayerHeight: number,
    ): Promise<void> {
        const data = {
            applicationId,
            displayerId,
            displayerWidth,
            displayerHeight,
        };

        return this.emit(MessageType.SESSION_READY, data);
    }

    emitSessionEnd(applicationId: string, displayerId: string): Promise<void> {
        const data = {
            applicationId,
            displayerId,
        };

        return this.emit(MessageType.SESSION_END, data);
    }

    emitApplicationData(data: object): Promise<void> {
        return this.emit(MessageType.APPLICATION_DATA, data);
    }

    emitQueue(queuePosition: number, queueTotal: number): Promise<void> {
        const data = {
            queuePosition,
            queueTotal,
        };

        return this.emit(MessageType.QUEUE, data);
    }

    emitMessage(
        title: string,
        content: string,
        level: Level,
    ): Promise<void> {
        const data = {
            title,
            content,
            level,
        };

        return this.emit(MessageType.MESSAGE, data);
    }

    onPlayerReady(callback: any): Promise<void> {
        return this.on(MessageType.PLAYER_READY, callback);
    }

    onPlayerData(callback: any): Promise<void> {
        return this.on(MessageType.PLAYER_DATA, callback);
    }

    onSessionEnd(callback: any): Promise<void> {
        return this.on(MessageType.SESSION_END, callback);
    }

    onDisconnect(callback: any): Promise<void> {
        return this.on(MessageType.DISCONNECT, callback);
    }

    private emit(
        message: MessageType,
        data: any,
    ) {
        const { socket } = this;

        return new Promise<void>((resolve, reject) => {
            if (socket != null) {
                socket.emit(message, data);
                resolve();
            } else {
                reject();
            }
        });
    }

    private on(message: MessageType, callback: any): Promise<void> {
        const { socket } = this;

        return new Promise((resolve, reject) => {
            if (socket != null) {
                socket.removeAllListeners(message);
                socket.on(message, callback);
                resolve();
            } else {
                reject();
            }
        });
    }
}
