import { Role } from '../utils/common/Role';
import { Status } from '../utils/common/Status';

export class UserModel {
    constructor(
        public username: string,
        public password: string,
        public role: Role,
        public status: Status,
    ) {
        // Empty
    }
}
