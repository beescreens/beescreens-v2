export class QueueElementModel {
    constructor(
        public playerId: string,
        public applicationId: string,
        public position: number,
    ) {
        // Empty
    }
}
