import { IDisplayer } from '../utils/common/IDisplayer';
import { Status } from '../utils/common/Status';
import { IEntity } from '../utils/common/IEntity';
import { Level } from '../utils/common/Level';
import { MessageType } from '../utils/common/MessageType';

export class DisplayerModel implements IEntity, IDisplayer {
    constructor(
        public id: string,
        public name: string,
        public location: string,
        public width: number,
        public height: number,
        public status: Status,
        public socket?: SocketIO.Socket,

    ) {
        // Empty
    }

    emitSessionGo(applicationId: string, playerId: string): Promise<void> {
        const data = {
            applicationId,
            playerId,
        };

        return this.emit(MessageType.SESSION_GO, data);
    }

    emitSessionReady(applicationId: string, playerId: string): Promise<void> {
        const data = {
            applicationId,
            playerId,
        };

        return this.emit(MessageType.SESSION_READY, data);
    }

    emitSessionEnd(applicationId: string, playerId: string): Promise<void> {
        const data = {
            applicationId,
            playerId,
        };

        return this.emit(MessageType.SESSION_END, data);
    }

    emitMessage(
        title: string,
        content: string,
        level: Level,
    ): Promise<void> {
        const data = {
            title,
            content,
            level,
        };

        return this.emit(MessageType.MESSAGE, data);
    }

    onDisplayerReady(callback: any): Promise<void> {
        return this.on(MessageType.DISPLAYER_READY, callback);
    }

    onSessionEnd(callback: any): Promise<void> {
        return this.on(MessageType.SESSION_END, callback);
    }

    onDisconnect(callback: any): Promise<void> {
        return this.on(MessageType.DISCONNECT, callback);
    }

    private emit(
        message: MessageType,
        data: any,
    ) {
        const { socket } = this;

        return new Promise<void>((resolve, reject) => {
            if (socket != null) {
                socket.emit(message, data);
                resolve();
            } else {
                reject();
            }
        });
    }

    private on(message: MessageType, callback: any): Promise<void> {
        const { socket } = this;

        return new Promise((resolve, reject) => {
            if (socket != null) {
                socket.removeAllListeners(message);
                socket.on(message, callback);
                resolve();
            } else {
                reject();
            }
        });
    }
}
