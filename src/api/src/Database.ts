import debug from 'debug';
import mongoose from 'mongoose';

const { connection } = mongoose;

const log = debug('beescreens:database');

class Database {
    dbConfig: object;

    constructor(
        public dbUsername: string,
        public dbPassword: string,
        public dbHostname: string,
        public dbPort: number,
        public dbName: string,
    ) {
        this.dbConfig = {
            useNewUrlParser: true,
            useCreateIndex: true,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 500,
            auth: {
                authSource: 'admin',
            },
        };
    }

    connect() {
        const {
            dbConfig,
            dbHostname,
            dbName,
            dbPassword,
            dbPort,
            dbUsername,
        } = this;

        return new Promise<void>((resolve) => {
            connection.once('open', () => {
                resolve();
            });

            const tryToConnect = () => {
                mongoose.connect(`mongodb://${dbUsername}:${dbPassword}@${dbHostname}:${dbPort}/${dbName}`, dbConfig)
                    .then(
                        () => {
                            log('Connection to database established.');
                        },
                        () => {
                            log('Connection to database failed, retrying...');
                            setTimeout(tryToConnect, 500);
                        },
                    );
            };

            tryToConnect();
        });
    }

    disconnect() {
        return new Promise<void>((resolve) => {
            mongoose.disconnect(() => {
                log('Grafully disconnected from the database.');
                resolve();
            });
        });
    }
}

export {
    Database,
    mongoose,
    connection,
};
