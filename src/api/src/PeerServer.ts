import debug from 'debug';
import { RestServer } from './RestServer';

const { ExpressPeerServer } = require('peer');

const log = debug('beescreens:peer-server');

export class PeerServer {
    instance: any;

    /**
     * The constructor.
     */
    constructor(public restServer: RestServer) {}

    /**
     * Start the server.
     */
    start() {
        const { restServer } = this;

        return new Promise<void>((resolve) => {
            const { express } = restServer;

            const instance = ExpressPeerServer(restServer.instance, {
                debug: false,
            });

            express.use('/peers', instance);

            instance.on('connection', (id: string) => {
                log(`Connection from '${id}'`);
            });

            instance.on('disconnect', (id: string) => {
                log(`Disconnect from '${id}'`);
            });

            this.instance = instance;

            log('Ready for connections');
            resolve();
        });
    }

    /**
     * Stop the server.
     */
    stop() {
        // Intervals are not correctly cleaned in this object and
        // and prevents from closing the programm gracefully.
        //
        // instance._setCleanupIntervals);
        return new Promise<void>((resolve) => {
            log('Server gracefully stopped.');
            resolve();
        });
    }
}
