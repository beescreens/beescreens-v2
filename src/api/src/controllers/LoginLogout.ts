import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { JwtService } from '../services/JwtService';
import { LoginLogoutService } from '../services/LoginLogoutService';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { Response401 } from '../utils/responses/Response401';
import { WrongUsernamePasswordError } from '../utils/errors/login/WrongUsernamePasswordError';
import { Token } from '../utils/common/Token';

const jwtService = container.resolve<JwtService>('jwtService');
const loginLogoutService = container.resolve<LoginLogoutService>('loginLogoutService');

const log = debug('beescreens:login-logout-controller');

export function login(req: any, res: Response) {
    let { credentials } = req.swagger.params;

    credentials = credentials.value;

    const { username, password } = credentials;

    log(`User '${username}' is trying to login in.`);

    loginLogoutService.login(username, password)
        .then(dto => jwtService.sign(
            new Token(dto.role, username),
        ))
        .then((token) => {
            log(`User '${username}' successfully logged in.`);
            Response200.send({
                jwt: token,
            }, res);
        })
        .catch(() => {
            log(`User '${username}' unable to log in.`);
            Response401.send({ message: WrongUsernamePasswordError.toString() }, res);
        });
}

export function logout(req: any, res: Response) {
    const authorization = req.auth;

    log(`A user is trying to log out with token ${authorization}.`);

    loginLogoutService.logout(authorization)
        .then(() => {
            log('User successfully logged out');
            Response204.send(res);
        })
        .catch(() => {
            log('User unable to log out');
            Response401.send({ message: WrongUsernamePasswordError }, res);
        });
}
