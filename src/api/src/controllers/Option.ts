import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';

import { OptionDTO } from '../dto/OptionDTO';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';
import { OptionService } from '../services/OptionService';

const optionService = container.resolve<OptionService>('optionService');

const log = debug('beescreens:option-controller');

export function changeOptionValue(req: any, res: Response) {
    let { optionId, option } = req.swagger.params;

    optionId = optionId.value;
    option = option.value;

    log(`Option value '${optionId}' will be updated.`);

    optionService.changeOptionValue(optionId, option.value)
        .then(() => {
            log(`Option value '${optionId}' has been successfully updated.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Option value '${optionId}' not found.`);
            Response404.send(res);
        });
}

export function getOption(req: any, res: Response) {
    let { optionId } = req.swagger.params;

    optionId = optionId.value;

    log(`Option '${optionId}' will be retrieved.`);

    optionService.getOption(optionId)
        .then((option) => {
            const optionDTO = new OptionDTO(option);

            log(`Option '${optionId}' has been successfully retrieved.`);
            Response200.send(optionDTO, res);
        })
        .catch(() => {
            log(`Option value '${optionId}' not found.`);
            Response404.send(res);
        });
}

export function getOptions(req: any, res: Response) {
    log(`Options will be retrieved.`);

    optionService.getOptions()
        .then((options) => {
            const optionsDTO = new Array<OptionDTO>();

            options.forEach((option) => {
                optionsDTO.push(new OptionDTO(option));
            });

            log(`Options have been successfully retrieved.`);
            Response200.send(optionsDTO, res);
        });
}
