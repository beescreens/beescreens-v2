import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { PlayerDTO } from '../dto/PlayerDTO';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';

import { PlayerService } from '../services/PlayerService';

const playerService = container.resolve<PlayerService>('playerService');

const log = debug('beescreens:player-controller');

export function deletePlayer(req: any, res: Response) {
    let { playerId } = req.swagger.params;

    playerId = playerId.value;

    log(`Player '${playerId}' will be deleted.`);

    playerService.deletePlayer(playerId)
        .then(() => {
            log(`Player '${playerId}' has been successfully deleted.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Player '${playerId}' not found.`);
            Response404.send(res);
        });
}

export function getPlayer(req: any, res: Response) {
    let { playerId } = req.swagger.params;

    playerId = playerId.value;

    log(`Player '${playerId}' will be retrieved.`);

    playerService.getPlayer(playerId)
        .then((player) => {
            const dto = new PlayerDTO(player);

            log(`Player '${playerId}' has been successfully retrieved.`);
            Response200.send(dto, res);
        })
        .catch(() => {
            log(`Player '${playerId}' not found.`);
            Response404.send(res);
        });
}

export function getPlayers(req: any, res: Response) {
    log(`Players will be retrieved.`);

    playerService.getPlayers()
        .then((players) => {
            const playersDTO = new Array<PlayerDTO>();

            players.forEach((player) => {
                playersDTO.push(new PlayerDTO(player));
            });

            log(`Players have been successfully retrieved.`);
            Response200.send(playersDTO, res);
        });
}

export function updatePlayer(req: any, res: Response) {
    let { playerId, player } = req.swagger.params;

    playerId = playerId.value;
    player = player.value;

    log(`Player '${playerId}' will be updated.`);

    playerService.updatePlayer(playerId, player.status)
        .then(() => {
            log(`Player '${playerId}' has been successfully updated.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Player '${playerId}' not found.`);
            Response404.send(res);
        });
}
