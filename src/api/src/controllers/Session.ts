import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { SessionDTO } from '../dto/SessionDTO';
import { SessionService } from '../services/SessionService';
import { Response404 } from '../utils/responses/Response404';

const sessionService = container.resolve<SessionService>('sessionService');

const log = debug('beescreens:session-controller');

export function deleteSession(req: any, res: Response) {
    let { sessionId } = req.swagger.params;

    sessionId = sessionId.value;

    log(`Session # '${sessionId}' will be deleted.`);

    sessionService.deleteSession(sessionId)
        .then(() => {
            log(`Session # '${sessionId}' has been successfully deleted.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Session # '${sessionId}' not found.`);
            Response404.send(res);
        });
}

export function getSession(req: any, res: Response) {
    let { sessionId } = req.swagger.params;

    sessionId = sessionId.value;

    log(`Session # '${sessionId}' will be retrieved.`);

    sessionService.getSession(sessionId)
        .then((session) => {
            const dto = new SessionDTO(session);

            log(`Session # '${sessionId}' has been successfully retrieved.`);
            Response200.send(dto, res);
        })
        .catch(() => {
            log(`Session # '${sessionId}' not found.`);
            Response404.send(res);
        });
}

export function getSessions(req: any, res: Response) {
    log(`Sessions will be retrieved.`);

    sessionService.getSessions()
        .then((sessions) => {
            const sessionsDTO = new Array<SessionDTO>();

            sessions.forEach((session) => {
                sessionsDTO.push(new SessionDTO(session));
            });

            log(`Sessions have been successfully retrieved.`);
            Response200.send(sessionsDTO, res);
        });
}
