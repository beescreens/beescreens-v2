import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { MessageService } from '../services/MessageService';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';

const messageService = container.resolve<MessageService>('messageService');

const log = debug('beescreens:message-controller');

export function sendMessage(req: any, res: Response) {
    let { message } = req.swagger.params;

    message = message.value;

    const { title, content, level } = message;

    log(`Message '${title}' will be sent to all entities.`);

    messageService.sendMessage(title, content, level)
        .then(() => {
            log(`Message '${title}' has successfully been sent to all entities.`);
            Response204.send(res);
        });
}

export function sendMessageToApplication(req: any, res: Response) {
    let { applicationId, message } = req.swagger.params;

    applicationId = applicationId.value;
    message = message.value;

    const { title, content, level } = message;

    log(`Message '${title}' will be sent to all entities playing '${applicationId}'.`);

    messageService.sendMessageToApplication(applicationId, title, content, level)
        .then(() => {
            log(`Message '${title}' has successfully been sent `
                + `to all entities playing '${applicationId}'.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Application '${applicationId}' not found.`);
            Response404.send(res);
        });
}

export function sendMessageToDisplayer(req: any, res: Response) {
    let {
        displayerId,
        message,
    } = req.swagger.params;

    displayerId = displayerId.value;
    message = message.value;

    const { title, content, level } = message;

    log(`Message '${title}' will be sent to displayer '${displayerId}'.`);

    messageService.sendMessageToDisplayer(displayerId, title, content, level)
        .then(() => {
            log(`Message '${title}' has been successfully sent to displayer '${displayerId}'.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Displayer '${displayerId}' not found.`);
            Response404.send(res);
        });
}

export function sendMessageToPlayer(req: any, res: Response) {
    let { playerId, message } = req.swagger.params;

    playerId = playerId.value;
    message = message.value;

    const { title, content, level } = message;

    log(`Message '${title}' will be sent to player '${playerId}'.`);

    messageService.sendMessageToPlayer(playerId, title, content, level)
        .then(() => {
            log(`Message '${title}' has been successfully sent to player '${playerId}'.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Player '${playerId}' not found.`);
            Response404.send(res);
        });
}
