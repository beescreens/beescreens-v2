import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';
import { Response409 } from '../utils/responses/Response409';
import { UserDTO } from '../dto/UserDTO';

import { UserService } from '../services/UserService';

const log = debug('beescreens:user-controller');

const userService = container.resolve<UserService>('userService');

export function addUser(req: any, res: Response) {
    let { user } = req.swagger.params;

    user = user.value;

    const {
        username,
        password,
        role,
    } = user;

    log(`User '${username}' will be added.`);

    userService.addUser(username, password, role)
        .then(() => {
            log(`User '${username}' has been successfully added.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`User with username '${username}' already exists.`);
            Response409.send(res);
        });
}

export function deleteUser(req: any, res: Response) {
    let { username } = req.swagger.params;

    username = username.value;

    log(`User '${username}' will be deleted.`);

    userService.deleteUser(username)
        .then(() => {
            log(`User '${username}' has been successfully deleted.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`User '${username}' not found.`);
            Response404.send(res);
        });
}

export function getUser(req: any, res: Response) {
    let { username } = req.swagger.params;

    username = username.value;

    log(`User '${username}' will be retrieved.`);

    userService.getUser(username)
        .then((user) => {
            const dto = new UserDTO(user);

            log(`User '${username}' has been successfully retrieved.`);
            Response200.send(dto, res);
        })
        .catch(() => {
            log(`User '${username}' not found.`);
            Response404.send(res);
        });
}

export function getUsers(req: any, res: Response) {
    log(`Users will be retrieved.`);

    userService.getUsers()
        .then((users) => {
            const usersDTO = new Array<UserDTO>();

            users.forEach((user) => {
                usersDTO.push(new UserDTO(user));
            });

            log(`Users have been successfully retrieved.`);
            Response200.send(usersDTO, res);
        });
}

export function updateUser(req: any, res: Response) {
    let { username, user } = req.swagger.params;

    username = username.value;
    user = user.value;

    log(`User '${username}' will be updated.`);

    userService.updateUser(username, user.password, user.role, user.status)
        .then(() => {
            log(`User '${username}' has been successfully updated.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`User '${username}' not found.`);
            Response404.send(res);
        });
}
