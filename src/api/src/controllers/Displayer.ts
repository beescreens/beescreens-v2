import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { DisplayerDTO } from '../dto/DisplayerDTO';
import { DisplayerService } from '../services/DisplayerService';
import { DontAcceptDisplayersError } from '../utils/errors/displayers/DontAcceptDisplayersError';
import { JwtService } from '../services/JwtService';
import { OnlyAcceptKnownDisplayersError } from '../utils/errors/displayers/OnlyAcceptKnownDisplayersError';
import { OnlyAcceptNewDisplayersError } from '../utils/errors/displayers/OnlyAcceptNewDisplayersError';
import { OptionService } from '../services/OptionService';
import { PasswordIsIncorrectError } from '../utils/errors/displayers/PasswordIsIncorrectError';
import { PasswordIsRequiredError } from '../utils/errors/displayers/PasswordIsRequiredError';
import { Response200 } from '../utils/responses/Response200';
import { Response201 } from '../utils/responses/Response201';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';
import { EntityType } from '../utils/common/EntityType';
import { Token } from '../utils/common/Token';
import { Response403 } from '../utils/responses/Response403';

const displayerService = container.resolve<DisplayerService>('displayerService');
const jwtService = container.resolve<JwtService>('jwtService');
const optionService = container.resolve<OptionService>('optionService');

const log = debug('beescreens:displayer-controller');

export function deleteDisplayer(req: any, res: Response) {
    let { displayerId } = req.swagger.params;

    displayerId = displayerId.value;

    log(`Displayer '${displayerId}' will be deleted.`);

    displayerService.deleteDisplayer(displayerId)
        .then(() => {
            log(`Displayer '${displayerId}' has been successfully deleted.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Displayer '${displayerId}' not found.`);
            Response404.send(res);
        });
}

export function getDisplayer(req: any, res: Response) {
    let { displayerId } = req.swagger.params;

    displayerId = displayerId.value;

    log(`Displayer '${displayerId}' will be retrieved.`);

    displayerService.getDisplayer(displayerId)
        .then((displayer) => {
            const dto = new DisplayerDTO(displayer);

            log(`Displayer '${displayerId}' has been successfully retrieved.`);
            Response200.send(dto, res);
        })
        .catch(() => {
            log(`Displayer '${displayerId}' not found.`);
            Response404.send(res);
        });
}

export function getDisplayers(req: any, res: Response) {
    log(`Displayers will be retrieved.`);

    displayerService.getDisplayers()
        .then((displayers) => {
            const displayersDTO = new Array<DisplayerDTO>();

            displayers.forEach((displayer) => {
                displayersDTO.push(new DisplayerDTO(displayer));
            });

            log(`Displayers have been successfully retrieved.`);
            Response200.send(displayersDTO, res);
        });
}

export function registerDisplayer(req: any, res: Response) {
    // We get the current values of the options
    Promise.all([
        optionService.getOption('accept_new_displayers'),
        optionService.getOption('accept_known_displayers'),
        optionService.getOption('displayers_password'),
    ]).then((options) => {
        // Parse the options
        const acceptNewDisplayers = JSON.parse(options[0].value);
        const acceptKnownDisplayers = JSON.parse(options[1].value);
        const displayersPassword = options[2].value;
        const requirePassword = displayersPassword.length !== 0;

        // Get the displayer's details from the request
        let { displayer } = req.swagger.params;

        displayer = displayer.value;

        const {
            id,
            name,
            location,
            width,
            height,
            password,
        } = displayer;

        log(`Displayer '${id}' would like to register.`);

        let isKnown: boolean;

        displayerService.getDisplayer(id)
            .then(() => {
                log(`Displayer '${id}' is already registered.`);
                isKnown = true;
            }).catch(() => {
                log(`Displayer '${id}' is not registered.`);
                isKnown = false;
            }).finally(() => {
                // Check if the displayer is accepted or not
                let needToCreate = false;
                let valid = false;

                if (
                    acceptKnownDisplayers
                    && isKnown
                ) {
                    // We don't accept new displayers,
                    // but we accept known displayers
                    // and it's known.
                    needToCreate = false;
                    valid = true;
                } else if (
                    acceptNewDisplayers
                    && !acceptKnownDisplayers
                    && !isKnown
                    && !requirePassword
                ) {
                    // We accept new displayers,
                    // but don't accept known displayers
                    // and it's not known
                    // and we don't require a password
                    needToCreate = true;
                    valid = true;
                } else if (
                    acceptNewDisplayers
                    && !acceptKnownDisplayers
                    && !isKnown
                    && requirePassword
                    && password != null
                    && password === displayersPassword
                ) {
                    // We accept new displayers,
                    // but don't accept known displayers,
                    // and it's not known
                    // and we require a password
                    // and the password is not null
                    // and the password is correct
                    needToCreate = true;
                    valid = true;
                } else if (
                    acceptNewDisplayers
                    && acceptKnownDisplayers
                    && isKnown
                    && requirePassword
                    && password != null
                    && password === displayersPassword
                ) {
                    // We accept new displayers,
                    // and we accept known displayers,
                    // and it's known
                    // and we require a password
                    // and the password is not null
                    // and the password is correct
                    needToCreate = false;
                    valid = true;
                } else if (
                    acceptNewDisplayers
                    && acceptKnownDisplayers
                    && !isKnown
                    && requirePassword
                    && password != null
                    && password === displayersPassword
                ) {
                    // We accept new displayers,
                    // and we accept known displayers,
                    // and it's not known
                    // and we require a password
                    // and the password is not null
                    // and the password is correct
                    needToCreate = true;
                    valid = true;
                } else if (
                    acceptNewDisplayers
                    && acceptKnownDisplayers
                    && isKnown
                    && !requirePassword
                ) {
                    // We accept new displayers,
                    // and we accept known displayers,
                    // and it's known
                    // and no password is required
                    needToCreate = false;
                    valid = true;
                } else if (
                    acceptNewDisplayers
                    && acceptKnownDisplayers
                    && !isKnown
                    && !requirePassword
                ) {
                    // We accept new displayers,
                    // and we accept known displayers,
                    // and it's not known
                    // and no password is required
                    needToCreate = true;
                    valid = true;
                }

                if (valid && needToCreate) {
                    log(`Displayer '${id}' will be registered.`);
                    displayerService.registerDisplayer(id, name, location, width, height)
                        .then(newDisplayer => jwtService.sign(
                            new Token(EntityType.DISPLAYER, newDisplayer.id),
                        ))
                        .then((token) => {
                            log(`Displayer '${id}' has been successfully registered.`);
                            Response201.send({
                                jwt: token,
                            }, res);
                        });
                } else if (valid && !needToCreate) {
                    log(`Displayer '${id}' do not need to register.`);
                    Response204.send(res);
                } else {
                    const response: any = {};

                    if (
                        !acceptNewDisplayers
                        && !acceptKnownDisplayers
                    ) {
                        // We don't accept new displayers,
                        // and we don't accept known displayers
                        response.message = DontAcceptDisplayersError.toString();
                    } else if (
                        !acceptNewDisplayers
                        && acceptKnownDisplayers
                        && !isKnown
                    ) {
                        // We don't accept new displayers,
                        // and we accept known displayers,
                        // and it's not known
                        response.message = OnlyAcceptKnownDisplayersError.toString();
                    } else if (
                        acceptNewDisplayers
                        && !acceptKnownDisplayers
                        && isKnown
                    ) {
                        // We accept new displayers,
                        // and we don't accept known displayers,
                        // and it's known
                        response.message = OnlyAcceptNewDisplayersError.toString();
                    } else if (
                        acceptNewDisplayers
                        && !acceptKnownDisplayers
                        && !isKnown
                        && requirePassword
                        && password == null
                    ) {
                        // We accept new displayers,
                        // and we don't accept known displayers,
                        // and it's not known
                        // and we required a password
                        // and password is null
                        response.message = PasswordIsRequiredError.toString();
                        response.requirePassword = true;
                    } else if (
                        acceptNewDisplayers
                        && !acceptKnownDisplayers
                        && !isKnown
                        && requirePassword
                        && password != null
                        && password !== displayersPassword
                    ) {
                        // We accept new displayers,
                        // and we don't accept known displayers,
                        // and it's not known
                        // and we required a password
                        // and password is not null
                        // and the password is incorrect
                        response.message = PasswordIsIncorrectError.toString();
                        response.requirePassword = true;
                    }

                    log(`Displayer '${id}' cannot register.`);
                    Response403.send(res);
                }
            });
    });
}

export function updateDisplayer(req: any, res: Response) {
    let {
        displayerId,
        displayer,
    } = req.swagger.params;

    displayerId = displayerId.value;
    displayer = displayer.value;

    log(`Displayer '${displayerId}' will be updated.`);

    displayerService.updateDisplayer(
        displayerId,
        displayer.status,
    )
        .then(() => {
            log(`Displayer '${displayerId}' has been successfully updated.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Displayer '${displayerId}' not found.`);
            Response404.send(res);
        });
}
