import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { ApplicationService } from '../services/ApplicationService';
import { EntityType } from '../utils/common/EntityType';
import { JwtService } from '../services/JwtService';
import { OptionService } from '../services/OptionService';
import { PlayService } from '../services/PlayService';
import { Response201 } from '../utils/responses/Response201';
import { Response403 } from '../utils/responses/Response403';
import { Response404 } from '../utils/responses/Response404';
import { Status } from '../utils/common/Status';
import { Token } from '../utils/common/Token';

const applicationService = container.resolve<ApplicationService>('applicationService');
const optionService = container.resolve<OptionService>('optionService');
const playService = container.resolve<PlayService>('playService');
const jwtService = container.resolve<JwtService>('jwtService');

const log = debug('beescreens:play-controller');

export function playApp(req: any, res: Response) {
    // Get the play's details from the request
    let { play } = req.swagger.params;

    play = play.value;

    const {
        applicationId,
        password,
    } = play;

    let acceptPlayers: boolean;
    let playersPassword: string;
    let requirePassword: boolean;
    let applicationAvailable: boolean;

    // We get the current values of the options
    Promise.all([
        optionService.getOption('accept_players'),
        optionService.getOption('players_password'),
    ])
        .then((options) => {
            // Parse the options
            acceptPlayers = JSON.parse(options[0].value);
            playersPassword = options[1].value;
            requirePassword = playersPassword.length !== 0;

            log(`Player would like to play '${applicationId}'.`);

            return applicationService.getApplication(applicationId);
        })
        .then((application) => {
            applicationAvailable = application.status === Status.ACTIVE;

            // Check if the player is accepted or not
            let valid = false;

            if (
                applicationAvailable
                && acceptPlayers
                && !requirePassword
            ) {
                // Application available,
                // we accept players,
                // and we don't require a password
                valid = true;
            } else if (
                applicationAvailable
                && acceptPlayers
                && requirePassword
                && password != null
                && password === playersPassword
            ) {
                // Application available,
                // we accept players,
                // and we require a password
                // and the password is not null
                // and the password is correct
                valid = true;
            }

            if (valid) {
                log(`Player will be able to play application '${applicationId}'`);

                playService.play(applicationId)
                    .then(newPlayer => jwtService.sign(
                        new Token(EntityType.PLAYER, newPlayer.id),
                    ))
                    .then((token) => {
                        Response201.send({
                            jwt: token,
                        }, res);
                    });
            } else {
                log(`Player will not be able to play application '${applicationId}'`);
                Response403.send(res);
            }
        })
        .catch(() => {
            log(`Application ${applicationId} not found.`);
            Response404.send(res);
        });
}
