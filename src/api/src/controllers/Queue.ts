import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { QueueElementDTO } from '../dto/QueueElementDTO';
import { QueueService } from '../services/QueueService';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';

const queueService = container.resolve<QueueService>('queueService');

const log = debug('beescreens:queue-controller');

export function deleteQueueElement(req: any, res: Response) {
    let { queueElementPosition } = req.swagger.params;

    queueElementPosition = queueElementPosition.value;

    log(`Queue element # '${queueElementPosition}' will be deleted.`);

    queueService.deleteQueueElement(queueElementPosition)
        .then(() => {
            log(`Queue element # '${queueElementPosition}' has been successfully deleted.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Queue element # '${queueElementPosition}' not found.`);
            Response404.send(res);
        });
}

export function getQueueElement(req: any, res: Response) {
    let { queueElementPosition } = req.swagger.params;

    queueElementPosition = queueElementPosition.value;

    log(`Queue element # '${queueElementPosition}' will be retrieved.`);

    queueService.getQueueElement(queueElementPosition)
        .then((queueElement) => {
            const dto = new QueueElementDTO(queueElement);

            log(`Queue element # '${queueElementPosition}' has been successfully retrieved.`);
            Response200.send(dto, res);
        })
        .catch(() => {
            log(`Queue element # '${queueElementPosition}' not found.`);
            Response404.send(res);
        });
}

export function getQueueElements(req: any, res: Response) {
    log(`Queue elements will be retrieved.`);

    queueService.getQueueElements()
        .then((queueElements) => {
            const queueElementsDTO = new Array<QueueElementDTO>();

            queueElements.forEach((queueElement) => {
                queueElementsDTO.push(new QueueElementDTO(queueElement));
            });

            log(`Queue elements have been successfully retrieved.`);
            Response200.send(queueElementsDTO, res);
        });
}

export function updateQueueElement(req: any, res: Response) {
    let { queueElementPosition, queueElement } = req.swagger.params;

    queueElementPosition = queueElementPosition.value;
    queueElement = queueElement.value;

    log(`Queue element # '${queueElementPosition}' will be updated.`);

    queueService.updateQueueElement(queueElementPosition, queueElement.position)
        .then(() => {
            log(`Queue element # '${queueElementPosition}' has been successfully updated.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Queue element # '${queueElementPosition}' not found.`);
            Response404.send(res);
        });
}
