import debug from 'debug';
import { Response } from 'express';

import { container } from '../container';
import { Response200 } from '../utils/responses/Response200';
import { Response204 } from '../utils/responses/Response204';
import { Response404 } from '../utils/responses/Response404';
import { ApplicationDTO } from '../dto/ApplicationDTO';
import { ApplicationService } from '../services/ApplicationService';
import { OptionService } from '../services/OptionService';
import { Response201 } from '../utils/responses/Response201';
import { PasswordIsRequiredError } from '../utils/errors/applications/PasswordIsRequiredError';
import { PasswordIsIncorrectError } from '../utils/errors/applications/PasswordIsIncorrectError';
import { Response403 } from '../utils/responses/Response403';
import { EntityType } from '../utils/common/EntityType';
import { JwtService } from '../services/JwtService';
import { Token } from '../utils/common/Token';
import { OnlyAcceptKnownApplicationsError } from '../utils/errors/applications/OnlyAcceptKnownApplicationsError';
import { DontAcceptApplicationsError } from '../utils/errors/applications/DontAcceptApplicationsError';
import { OnlyAcceptNewApplicationsError } from '../utils/errors/applications/OnlyAcceptNewApplicationsError';

const applicationService = container.resolve<ApplicationService>('applicationService');
const jwtService = container.resolve<JwtService>('jwtService');
const optionService = container.resolve<OptionService>('optionService');

const log = debug('beescreens:application-controller');

export function deleteApplication(req: any, res: Response) {
    let { applicationId } = req.swagger.params;

    applicationId = applicationId.value;

    log(`Application '${applicationId}' will be deleted.`);

    applicationService.deleteApplication(applicationId)
        .then(() => {
            log(`Application '${applicationId}' has been successfully deleted.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Application '${applicationId}' not found.`);
            Response404.send(res);
        });
}

export function getApplication(req: any, res: Response) {
    let { applicationId } = req.swagger.params;

    applicationId = applicationId.value;

    log(`Application '${applicationId}' will be retrieved.`);

    applicationService.getApplication(applicationId)
        .then((app) => {
            const dto = new ApplicationDTO(app);

            log(`Application '${applicationId}' has been successfully retrieved.`);
            Response200.send(dto, res);
        })
        .catch(() => {
            log(`Application '${applicationId}' not found.`);
            Response404.send(res);
        });
}

export function getApplications(req: any, res: Response) {
    log(`Applications will be retrieved.`);

    applicationService.getApplications()
        .then((applications) => {
            const applicationsDTO = new Array<ApplicationDTO>();

            applications.forEach((application) => {
                applicationsDTO.push(new ApplicationDTO(application));
            });

            log(`Applications have been successfully retrieved.`);
            Response200.send(applicationsDTO, res);
        });
}

export function registerApplication(req: any, res: Response) {
    // We get the current values of the options
    Promise.all([
        optionService.getOption('accept_new_applications'),
        optionService.getOption('accept_known_applications'),
        optionService.getOption('applications_password'),
    ]).then((options) => {
        // Parse the options
        const acceptNewApplications = JSON.parse(options[0].value);
        const acceptKnownApplications = JSON.parse(options[1].value);
        const applicationsPassword = options[2].value;
        const requirePassword = applicationsPassword.length !== 0;

        // Get the application's details from the request
        let { application } = req.swagger.params;

        application = application.value;

        const {
            id,
            name,
            description,
            logo,
            homepage,
            documentation,
            version,
            authors,
            password,
        } = application;

        log(`Application '${id}' would like to register.`);

        let isKnown: boolean;

        applicationService.getApplication(id)
            .then(() => {
                log(`Application '${id}' is already registered.`);
                isKnown = true;
            }).catch(() => {
                log(`Application '${id}' is not registered.`);
                isKnown = false;
            }).finally(() => {
                // Check if the application is accepted or not
                let needToCreate = false;
                let valid = false;

                if (
                    acceptKnownApplications
                    && isKnown
                ) {
                    // We don't accept new applications,
                    // but we accept known applications
                    // and it's known.
                    needToCreate = false;
                    valid = true;
                } else if (
                    acceptNewApplications
                    && !acceptKnownApplications
                    && !isKnown
                    && !requirePassword
                ) {
                    // We accept new applications,
                    // but don't accept known applications
                    // and it's not known
                    // and we don't require a password
                    needToCreate = true;
                    valid = true;
                } else if (
                    acceptNewApplications
                    && !acceptKnownApplications
                    && !isKnown
                    && requirePassword
                    && password != null
                    && password === applicationsPassword
                ) {
                    // We accept new applications,
                    // but don't accept known applications,
                    // and it's not known
                    // and we require a password
                    // and the password is not null
                    // and the password is correct
                    needToCreate = true;
                    valid = true;
                } else if (
                    acceptNewApplications
                    && acceptKnownApplications
                    && isKnown
                    && requirePassword
                    && password != null
                    && password === applicationsPassword
                ) {
                    // We accept new applications,
                    // and we accept known applications,
                    // and it's known
                    // and we require a password
                    // and the password is not null
                    // and the password is correct
                    needToCreate = false;
                    valid = true;
                } else if (
                    acceptNewApplications
                    && acceptKnownApplications
                    && !isKnown
                    && requirePassword
                    && password != null
                    && password === applicationsPassword
                ) {
                    // We accept new applications,
                    // and we accept known applications,
                    // and it's not known
                    // and we require a password
                    // and the password is not null
                    // and the password is correct
                    needToCreate = true;
                    valid = true;
                } else if (
                    acceptNewApplications
                    && acceptKnownApplications
                    && isKnown
                    && !requirePassword
                ) {
                    // We accept new applications,
                    // and we accept known applications,
                    // and it's known
                    // and no password is required
                    needToCreate = false;
                    valid = true;
                } else if (
                    acceptNewApplications
                    && acceptKnownApplications
                    && !isKnown
                    && !requirePassword
                ) {
                    // We accept new applications,
                    // and we accept known applications,
                    // and it's not known
                    // and no password is required
                    needToCreate = true;
                    valid = true;
                }

                if (valid && needToCreate) {
                    log(`Application '${id}' will be registered.`);
                    applicationService.registerApplication(
                        id,
                        name,
                        description,
                        logo,
                        homepage,
                        documentation,
                        version,
                        authors,
                    )
                        .then(newApplication => jwtService.sign(
                            new Token(EntityType.APPLICATION, newApplication.id),
                        ))
                        .then((token) => {
                            log(`Application '${id}' has been successfully registered.`);
                            Response201.send({
                                jwt: token,
                            }, res);
                        });
                } else if (valid && !needToCreate) {
                    log(`Application '${id}' do not need to register.`);
                    Response204.send(res);
                } else {
                    const response: any = {};

                    if (
                        !acceptNewApplications
                        && !acceptKnownApplications
                    ) {
                        // We don't accept new applications,
                        // and we don't accept known applications
                        response.message = DontAcceptApplicationsError.toString();
                    } else if (
                        !acceptNewApplications
                        && acceptKnownApplications
                        && !isKnown
                    ) {
                        // We don't accept new applications,
                        // and we accept known applications,
                        // and it's not known
                        response.message = OnlyAcceptKnownApplicationsError.toString();
                    } else if (
                        acceptNewApplications
                        && !acceptKnownApplications
                        && isKnown
                    ) {
                        // We accept new applications,
                        // and we don't accept known applications,
                        // and it's known
                        response.message = OnlyAcceptNewApplicationsError.toString();
                    } else if (
                        acceptNewApplications
                        && !acceptKnownApplications
                        && !isKnown
                        && requirePassword
                        && password == null
                    ) {
                        // We accept new applications,
                        // and we don't accept known applications,
                        // and it's not known
                        // and we required a password
                        // and password is null
                        response.message = PasswordIsRequiredError.toString();
                        response.requirePassword = true;
                    } else if (
                        acceptNewApplications
                        && !acceptKnownApplications
                        && !isKnown
                        && requirePassword
                        && password != null
                        && password !== applicationsPassword
                    ) {
                        // We accept new applications,
                        // and we don't accept known applications,
                        // and it's not known
                        // and we required a password
                        // and password is not null
                        // and the password is incorrect
                        response.message = PasswordIsIncorrectError.toString();
                        response.requirePassword = true;
                    }

                    log(`Application '${id}' cannot register.`);
                    Response403.send(res);
                }
            });
    });
}

export function updateApplication(req: any, res: Response) {
    let { applicationId, application } = req.swagger.params;

    applicationId = applicationId.value;
    application = application.value;

    log(`Application '${applicationId}' will be updated.`);

    applicationService.updateApplication(applicationId, application.status)
        .then(() => {
            log(`Application '${applicationId}' has been successfully updated.`);
            Response204.send(res);
        })
        .catch(() => {
            log(`Application '${applicationId}' not found.`);
            Response404.send(res);
        });
}
