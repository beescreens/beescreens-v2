const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

import debug from 'debug';

import { container } from './container';
import { Database } from './Database';
import { IoServer } from './IoServer';
import { PeerServer } from './PeerServer';
import { RestServer } from './RestServer';
import { Role } from './utils/common/Role';
import { UserService } from './services/UserService';

const adminUsername: string = container.cradle.adminUsername;
const adminPassword: string = container.cradle.adminPassword;
const database: Database = container.cradle.database;
const ioServer: IoServer = container.cradle.ioServer;
const peerServer: PeerServer = container.cradle.peerServer;
const restServer: RestServer = container.cradle.restServer;
const userService: UserService = container.cradle.userService;

const log = debug('beescreens:index');

// Start the API
database.connect()
    .then(() => {
        log(`Connected to database.`);

        return userService.getUser(adminUsername);
    })
    .catch(() => {
        log(`User '${adminUsername}' will be added.`);

        return userService.addUser(
            adminUsername,
            adminPassword,
            Role.ADMIN,
        );
    })
    .catch(() => {
        log(`User '${adminUsername}' cannot be added.`);
    })
    .finally(() => {
        log(`User '${adminUsername}' already exists.`);

        restServer.start();
        ioServer.start();
        peerServer.start();
    });
