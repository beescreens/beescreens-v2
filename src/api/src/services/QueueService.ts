import debug from 'debug';
import FastPriorityQueue from 'fastpriorityqueue';

import { QueueElementModel } from '../models/QueueElementModel';
import { PlayerService } from './PlayerService';

const log = debug('beescreens:queue-service');

export class QueueService {
    queue: FastPriorityQueue<QueueElementModel>;
    nextPosition: number;

    constructor(
        public playerService: PlayerService,
    ) {
        const queue = new FastPriorityQueue<QueueElementModel>(
            (
                e1: QueueElementModel,
                e2: QueueElementModel,
            ) => e1.position < e2.position,
        );

        this.queue = queue;
        this.nextPosition = 1;
    }

    deleteQueueElement(queueElementPosition: number) {
        const { queue } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Queue element # '${queueElementPosition}' will be deleted.`);

            this.getQueueElement(queueElementPosition)
                .then((queueElement) => {
                    queue.remove(queueElement);

                    return this.notifyPlayersInQueue();
                })
                .then(() => {
                    log(`Queue element # '${queueElementPosition}' has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    log(`Queue element # '${queueElementPosition}' not found.`);
                    reject();
                });
        });
    }

    getQueueElement(queueElementPosition: number) {
        const { queue } = this;

        return new Promise<QueueElementModel>((resolve, reject) => {
            log(`Queue element # '${queueElementPosition}' will be retrieved.`);

            let elementFound = null;

            queue.forEach((queueElement) => {
                if (queueElement.position === queueElementPosition) {
                    elementFound = queueElement;

                    log(`Queue element # '${queueElementPosition}' `
                        + `has been successfully retrieved.`);
                    resolve(elementFound);
                }
            });

            if (elementFound == null) {
                log(`Queue element # '${queueElementPosition}' not found.`);
                reject();
            }
        });
    }

    getQueueElements() {
        const { queue } = this;

        return new Promise<Array<QueueElementModel>>((resolve) => {
            const queueElements = new Array<QueueElementModel>();

            queue.forEach((queueElement) => {
                queueElements.push(queueElement);
            });

            log(`Queue elements have been successfully retrieved.`);
            resolve(queueElements);
        });
    }

    updateQueueElement(
        queueElementPosition: number,
        newPosition: number,
    ) {
        const { queue } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Queue element # '${queueElementPosition}' will be updated.`);

            this.getQueueElement(queueElementPosition)
                .then((queueElement) => {
                    queueElement.position = newPosition;

                    this.deleteQueueElement(queueElementPosition)
                        .then(() => {
                            queue.add(queueElement);

                            log(`Queue element # '${queueElementPosition}' `
                                + `has been successfully updated.`);
                            resolve();
                        });
                })
                .catch(() => {
                    log(`Queue element # '${queueElementPosition}' not found.`);
                    reject();
                });
        });
    }

    notifyPlayersInQueue() {
        const {
            playerService,
            queue,
        } = this;

        return new Promise<void>((resolve) => {
            log('Players positions will be updated.');
            const queueTotal = queue.size;

            let currentPosition = 1;

            queue.forEach((queueElement) => {
                playerService
                    .getPlayer(queueElement.playerId)
                    .then((player) => {
                        player.emitQueue(
                            currentPosition,
                            queueTotal,
                        );

                        currentPosition += 1;
                    });
            });


            log('Players positions have been successfully updated.');
            resolve();
        });
    }

    addNormalPlayer(
        playerId: string,
        applicationId: string,
    ) {
        const { nextPosition } = this;

        return new Promise<QueueElementModel>((resolve) => {
            log(`Player '${playerId}' will be added to queue to play '${applicationId}'.`);

            this.addVipPlayer(playerId, applicationId, nextPosition)
                .then((queueElement) => {
                    this.nextPosition += 1;

                    log(`Player '${playerId}' has been added `
                     + `to queue to play '${applicationId}'.`);
                    resolve(queueElement);
                });
        });
    }

    addVipPlayer(
        playerId: string,
        applicationId: string,
        position: number,
    ) {
        const { queue } = this;

        log(`Player '${playerId}' will be added to `
            + `queue to play '${applicationId}'. at position ${position}`);

        return new Promise<QueueElementModel>((resolve) => {
            const newQueueElement = new QueueElementModel(playerId, applicationId, position);

            queue.add(newQueueElement);

            this.notifyPlayersInQueue()
                .then(() => {
                    log(`Player '${playerId}' has been added to `
                        + `queue to play '${applicationId}' at position ${position}.`);
                    resolve(newQueueElement);
                });
        });
    }

    getNextQueueElement() {
        const { queue } = this;

        return new Promise<QueueElementModel>((resolve) => {
            log('The next player will be polled from the queue.');
            const queueElement = queue.poll();

            this.notifyPlayersInQueue()
                .then(() => {
                    log('The next player has been polled from the queue.');
                    resolve(queueElement);
                });
        });
    }
}
