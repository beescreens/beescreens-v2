import debug from 'debug';
import { DisplayerModel } from '../models/DisplayerModel';
import { Status } from '../utils/common/Status';

const log = debug('beescreens:displayer-service');

export class DisplayerService {
    inactiveDisplayers: Map<string, DisplayerModel>;
    activeDisplayers: Map<string, DisplayerModel>;

    constructor() {
        const inactiveDisplayers = new Map<string, DisplayerModel>();
        const activeDisplayers = new Map<string, DisplayerModel>();

        this.inactiveDisplayers = inactiveDisplayers;
        this.activeDisplayers = activeDisplayers;
    }

    deleteDisplayer(displayerId: string) {
        const { inactiveDisplayers, activeDisplayers } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Displayer '${displayerId}' will be deleted.`);

            this.getDisplayer(displayerId)
                .then(() => {
                    inactiveDisplayers.delete(displayerId);
                    activeDisplayers.delete(displayerId);

                    log(`Displayer '${displayerId}' has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    log(`Displayer '${displayerId}' not found.`);
                    reject();
                });
        });
    }

    getDisplayer(displayerId: string) {
        const {
            inactiveDisplayers,
            activeDisplayers,
        } = this;

        return new Promise<DisplayerModel>((resolve, reject) => {
            log(`Displayer '${displayerId}' will be retrieved.`);
            const displayers = new Map([...inactiveDisplayers, ...activeDisplayers]);

            const displayer = displayers.get(displayerId);

            if (displayer != null) {
                log(`Displayer '${displayerId}' has been successfully retrieved.`);
                resolve(displayer);
            } else {
                log(`Displayer '${displayerId}' not found.`);
                reject();
            }
        });
    }

    getDisplayers() {
        return new Promise<Array<DisplayerModel>>((resolve) => {
            Promise.all([
                this.getActiveDisplayers(),
                this.getInactiveDisplayers(),
            ])
                .then((displayers) => {
                    log(`Displayers have been successfully retrieved.`);
                    resolve([...displayers[0], ...displayers[1]]);
                });
        });
    }

    registerDisplayer(
        id: string,
        name: string,
        location: string,
        width: number,
        height: number,
    ) {
        const {
            activeDisplayers,
        } = this;

        return new Promise<DisplayerModel>((resolve) => {
            const displayer = new DisplayerModel(
                id,
                name,
                location,
                width,
                height,
                Status.ACTIVE,
            );

            activeDisplayers.set(id, displayer);

            log(`Displayer '${id}' has been successfully registered.`);
            resolve(displayer);
        });
    }

    updateDisplayer(
        displayerId: string,
        status: Status,
    ) {
        const {
            inactiveDisplayers,
            activeDisplayers,
        } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Displayer '${displayerId}' will be updated.`);
            this.getDisplayer(displayerId)
                .then((displayer) => {
                    this.deleteDisplayer(displayer.id)
                        .then(() => {
                            if (status === Status.INACTIVE) {
                                displayer.status = Status.INACTIVE;
                                inactiveDisplayers.set(displayer.id, displayer);
                            } else {
                                displayer.status = Status.ACTIVE;
                                activeDisplayers.set(displayer.id, displayer);
                            }

                            log(`Displayer '${displayerId}' has been successfully updated.`);

                            resolve();
                        });
                })
                .catch(() => {
                    log(`Displayer '${displayerId}' not found.`);
                    reject();
                });
        });
    }

    getActiveDisplayers() {
        const {
            activeDisplayers,
        } = this;

        return new Promise<Array<DisplayerModel>>((resolve) => {
            resolve([...activeDisplayers.values()]);
        });
    }

    getInactiveDisplayers() {
        const {
            inactiveDisplayers,
        } = this;

        return new Promise<Array<DisplayerModel>>((resolve) => {
            resolve([...inactiveDisplayers.values()]);
        });
    }

    borrowDisplayer() {
        const { inactiveDisplayers } = this;

        return new Promise<DisplayerModel>((resolve, reject) => {
            log('The next available displayer will be retreived.');
            if (inactiveDisplayers.size > 0) {
                const displayer = inactiveDisplayers.values().next().value;

                this.updateDisplayer(displayer.id, Status.ACTIVE)
                    .then(() => {
                        resolve(displayer);
                    })
                    .catch(() => {
                        reject();
                    });
            } else {
                reject();
            }
        });
    }

    restituteDisplayer(displayerId: string) {
        return this.updateDisplayer(displayerId, Status.INACTIVE);
    }
}
