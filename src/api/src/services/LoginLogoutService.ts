import debug from 'debug';

import { JwtService } from './JwtService';
import { UserService } from './UserService';
import { UserModel } from '../models/UserModel';

const log = debug('beescreens:login-logout-service');

export class LoginLogoutService {
    constructor(
        public jwtService: JwtService,
        public userService: UserService,
    ) {
        // Empty
    }

    login(
        username: string,
        password: string,
    ) {
        const { userService } = this;

        return new Promise<UserModel>((resolve, reject) => {
            log(`User '${username}' is trying to login in.`);

            userService.getUser(username)
                .then((user) => {
                    if (user.password !== password) {
                        log(`User '${username}' unable to log in.`);
                        reject();
                    }

                    log(`User '${username}' successfully logged in.`);
                    resolve(user);
                })
                .catch(() => {
                    reject();
                });
        });
    }

    logout(token: string) {
        const { jwtService } = this;

        return new Promise<void>((resolve, reject) => {
            log('A user is trying to log out.');

            jwtService.isJwtKnown(token)
                .then(() => jwtService.addBlacklistedJwt(token))
                .then(() => {
                    log('A user has successfully logged out.');
                    resolve();
                })
                .catch(() => {
                    log('User unable to log out');
                    reject();
                });
        });
    }
}
