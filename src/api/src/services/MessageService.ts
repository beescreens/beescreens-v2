import debug from 'debug';
import { ApplicationService } from './ApplicationService';
import { DisplayerService } from './DisplayerService';
import { PlayerService } from './PlayerService';
import { Level } from '../utils/common/Level';

const log = debug('beescreens:message-service');

export class MessageService {
    constructor(
        public applicationService: ApplicationService,
        public displayerService: DisplayerService,
        public playerService: PlayerService,
    ) {
        // Empty
    }

    sendMessage(
        title: string,
        content: string,
        level: Level,
    ) {
        const {
            applicationService,
            displayerService,
            playerService,
        } = this;

        return new Promise((resolve) => {
            log(`Message '${title}' will be sent to all entities.`);

            Promise.all([
                applicationService.getApplications(),
                displayerService.getDisplayers(),
                playerService.getPlayers(),
            ])
                .then((entities) => {
                    const applications = entities[0];
                    const displayers = entities[1];
                    const players = entities[2];

                    applications.forEach((application) => {
                        application.emitMessage(title, content, level);
                    });

                    displayers.forEach((displayer) => {
                        displayer.emitMessage(title, content, level);
                    });

                    players.forEach((player) => {
                        player.emitMessage(title, content, level);
                    });

                    log(`Message '${title}' has successfully been sent to all entities.`);
                    resolve();
                });
        });
    }

    sendMessageToApplication(
        applicationId: string,
        title: string,
        content: string,
        level: Level,
    ) {
        const { applicationService } = this;

        return new Promise((resolve, reject) => {
            log(`Message '${title}' will be sent to all entities playing '${applicationId}'.`);

            applicationService.getApplication(applicationId)
                .then((application) => {
                    application.emitMessage(title, content, level);

                    log(`Message '${title}' has successfully been sent `
                        + `to all entities playing '${applicationId}'.`);
                    resolve();
                })
                .catch(() => {
                    log(`Application '${applicationId}' not found.`);
                    reject();
                });
        });
    }

    sendMessageToDisplayer(
        displayerId: string,
        title: string,
        content: string,
        level: Level,
    ) {
        const { displayerService } = this;

        return new Promise((resolve, reject) => {
            log(`Message '${title}' will be sent to displayer '${displayerId}'.`);

            displayerService.getDisplayer(displayerId)
                .then((displayer) => {
                    displayer.emitMessage(title, content, level);

                    log(`Message '${title}' has been `
                        + `successfully sent to displayer '${displayerId}'.`);
                    resolve();
                })
                .catch(() => {
                    log(`Displayer '${displayerId}' not found.`);
                    reject();
                });
        });
    }

    sendMessageToPlayer(
        playerId: string,
        title: string,
        content: string,
        level: Level,
    ) {
        const { playerService } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Message '${title}' will be sent to player '${playerId}'.`);

            playerService.getPlayer(playerId)
                .then((player) => {
                    player.emitMessage(
                        title,
                        content,
                        level,
                    );

                    log(`Message '${title}' has been successfully sent to player '${playerId}'.`);
                    resolve();
                })
                .catch(() => {
                    log(`Player '${playerId}' not found.`);
                    reject();
                });
        });
    }
}
