// import debug from 'debug';
import uniqid from 'uniqid';

import { ApplicationService } from './ApplicationService';
import { DisplayerService } from './DisplayerService';
import { PlayerModel } from '../models/PlayerModel';
import { PlayerService } from './PlayerService';
import { QueueElementModel } from '../models/QueueElementModel';
import { QueueService } from './QueueService';
import { SessionService } from './SessionService';

// const log = debug('beescreens:play-service');

export class PlayService {
    reservations: Map<string, string>;

    constructor(
        public applicationService: ApplicationService,
        public displayerService: DisplayerService,
        public playerService: PlayerService,
        public queueService: QueueService,
        public sessionService: SessionService,
    ) {
        this.reservations = new Map<string, string>();
    }

    play(
        applicationId: string,
    ) {
        const {
            playerService,
            reservations,
        } = this;

        return new Promise<PlayerModel>((resolve, reject) => {
            const id = uniqid();

            playerService.registerPlayer(id)
                .then((player) => {
                    reservations.set(player.id, applicationId);
                    resolve(player);
                });
        });
    }

    joinQueue(
        playerId: string,
    ) {
        const {
            reservations,
            queueService,
        } = this;

        return new Promise<QueueElementModel>((resolve) => {
            const applicationId = reservations.get(playerId);

            reservations.delete(playerId);

            if (applicationId != null) {
                queueService.addNormalPlayer(
                    playerId,
                    applicationId,
                )
                    .then((queueElement) => {
                        resolve(queueElement);
                    });
            }
        });
    }
}
