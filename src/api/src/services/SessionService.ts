import debug from 'debug';

import { ApplicationModel } from '../models/ApplicationModel';
import { ApplicationService } from './ApplicationService';
import { DisplayerModel } from '../models/DisplayerModel';
import { DisplayerService } from './DisplayerService';
import { PlayerModel } from '../models/PlayerModel';
import { PlayerService } from './PlayerService';
import { QueueService } from './QueueService';
import { SessionModel } from '../models/SessionModel';
import { Status } from '../utils/common/Status';

const log = debug('beescreens:session-service');

export class SessionService {
    sessions: Map<string, SessionModel>;

    constructor(
        public applicationService: ApplicationService,
        public displayerService: DisplayerService,
        public playerService: PlayerService,
        public queueService: QueueService,
    ) {
        const sessions = new Map<string, SessionModel>();

        this.sessions = sessions;
    }

    deleteSession(sessionId: string) {
        const { sessions } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Session # '${sessionId}' will be deleted.`);

            this.getSession(sessionId)
                .then((session) => {
                    const { player, application, displayer } = session;

                    log(`Session for player '${player.id}', application `
                        + `'${application.id}' and displayer '${displayer.id} will end.`);

                    return Promise.all([
                        this.askPlayerToEndSession(session),
                        this.askDisplayerToEndSession(session),
                        this.askApplicationToEndSession(session),
                    ]);
                })
                .then(() => {
                    sessions.delete(sessionId);

                    this.askToStartSession()
                        .catch(() => {
                            // Do nothing
                        });

                    log(`Session # '${sessionId}' has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    log(`Session # '${sessionId}' not found.`);
                    reject();
                });
        });
    }

    getSession(sessionId: string) {
        const { sessions } = this;

        return new Promise<SessionModel>((resolve, reject) => {
            log(`Session # '${sessionId}' will be retrieved.`);

            const session = sessions.get(sessionId);

            if (session != null) {
                log(`Session # '${sessionId}' has been successfully retrieved.`);
                resolve(session);
            } else {
                log(`Session # '${sessionId}' not found.`);
                reject();
            }
        });
    }

    getSessions() {
        const { sessions } = this;

        return new Promise<Array<SessionModel>>((resolve) => {
            log(`Sessions have been successfully retrieved.`);
            resolve([...sessions.values()]);
        });
    }

    askToStartSession() {
        const {
            applicationService,
            displayerService,
            playerService,
            queueService,
        } = this;

        return new Promise((resolve, reject) => {
            log('Ask to start session');

            Promise.all([
                displayerService.getInactiveDisplayers(),
                displayerService.getActiveDisplayers(),
                queueService.getQueueElements(),
            ])
                .then((entities) => {
                    const nbOfInactiveDisplayers = entities[0].length;
                    const nbOfActiveDisplayers = entities[1].length;
                    const nbOfWaitingPlayers = entities[2].length;

                    log(`Number of inactive displayers: ${nbOfInactiveDisplayers}`);
                    log(`Number of active displayers: ${nbOfActiveDisplayers}`);
                    log(`Number of waiting players: ${nbOfWaitingPlayers}`);

                    if (nbOfInactiveDisplayers > 0 && nbOfWaitingPlayers > 0) {
                        return queueService.getNextQueueElement();
                    }
                })
                .then((queueElement) => {
                    const {
                        applicationId,
                        playerId,
                    } = queueElement;

                    return Promise.all([
                        applicationService.getApplication(applicationId),
                        displayerService.borrowDisplayer(),
                        playerService.getPlayer(playerId),
                    ]);
                })
                .then((entities) => {
                    const application = entities[0];
                    const displayer = entities[1];
                    const player = entities[2];

                    return this.createSession(player, application, displayer);
                })
                .then(() => {
                    log(`Session can be started.`);
                    resolve();
                })
                .catch(() => {
                    log('Session cannot be started now.');
                    reject();
                });
        });
    }

    createSession(
        player: PlayerModel,
        application: ApplicationModel,
        displayer: DisplayerModel,
    ) {
        const {
            applicationService,
            displayerService,
            playerService,
            sessions,
        } = this;

        return new Promise<void>((resolve) => {
            const session = new SessionModel(player, application, displayer);

            const { id } = session;

            log(`Session #${id} will link application '${application.id}', `
                + `displayer '${displayer.id}' and player '${player.id}'`);

            sessions.set(id, session);

            application.onSessionEnd(() => {
                this.deleteSession(id);
            });

            displayer.onSessionEnd(() => {
                this.deleteSession(id);
            });

            player.onSessionEnd(() => {
                this.deleteSession(id);
            });

            application.onDisconnect(() => {
                Promise.all([
                    this.askDisplayerToEndSession(session),
                    this.askPlayerToEndSession(session),
                    applicationService.updateApplication(application.id, Status.INACTIVE),
                ])
                    .then(() => {
                        this.deleteSession(id)
                            .catch(() => {
                                // Nothing;
                            });
                    })
                    .then(() => {
                        this.askToStartSession()
                            .catch(() => {
                                // Nothing
                            });
                    });
            });

            displayer.onDisconnect(() => {
                Promise.all([
                    this.askApplicationToEndSession(session),
                    this.askPlayerToEndSession(session),
                    displayerService.updateDisplayer(displayer.id, Status.INACTIVE),
                ])
                    .then(() => {
                        this.deleteSession(id)
                            .catch(() => {
                                // Nothing;
                            });
                    })
                    .then(() => {
                        this.askToStartSession()
                            .catch(() => {
                                // Nothing
                            });
                    });
            });

            player.onDisconnect(() => {
                Promise.all([
                    this.askApplicationToEndSession(session),
                    this.askDisplayerToEndSession(session),
                    playerService.deletePlayer(player.id),
                ])
                    .then(() => {
                        this.deleteSession(id)
                            .catch(() => {
                                // Nothing;
                            });
                    })
                    .then(() => {
                        this.askToStartSession()
                            .catch(() => {
                                // Nothing
                            });
                    })
                    .catch(() => {
                        // Nothing;
                    });
            });

            player.onPlayerData((data: object) => {
                log(`Session #${id}: player '${player.id}' is sending data `
                    + `to application '${application.id}'.`);
                application.emitPlayerData(player.id, data);
            });

            application.onApplicationData((data: object) => {
                log(`Session #${id}: application '${application.id}' is sending `
                    + `data to player '${player.id}'.`);
                player.emitApplicationData(data);
            });

            let applicationReady = false;
            let displayerReady = false;
            let playerReady = false;

            Promise.all([
                application.onApplicationReady(() => {
                    log(`Session #${id}: application '${application.id}' ready.`);
                    applicationReady = true;
                }),
                displayer.onDisplayerReady(() => {
                    log(`Session #${id}: displayer '${displayer.id}' ready.`);
                    displayerReady = true;
                }),
                player.onPlayerReady(() => {
                    log(`Session #${id}: player '${player.id}' ready.`);
                    playerReady = true;
                }),
            ])
                .then(() => {
                    return Promise.all([
                        application.emitSessionReady(
                            displayer.id,
                            displayer.width,
                            displayer.height,
                            player.id,
                        ),
                        displayer.emitSessionReady(
                            player.id,
                            application.id,
                        ),
                        player.emitSessionReady(
                            application.id,
                            displayer.id,
                            displayer.width,
                            displayer.height,
                        ),
                    ]);
                })
                .then(() => {
                    const interval = setInterval(() => {
                        if (applicationReady && displayerReady && playerReady) {
                            clearInterval(interval);
                            return Promise.all([
                                player.emitSessionGo(application.id, displayer.id),
                                application.emitSessionGo(displayer.id, player.id),
                                displayer.emitSessionGo(application.id, player.id),
                            ]);
                        }
                    }, 200);
                })
                .then(() => {
                    log(`Player '${player.id}' will play '${application.id}' `
                        + `on displayer '${displayer.id}`);
                    resolve();
                });
        });
    }

    askApplicationToEndSession(session: SessionModel) {
        return new Promise<void>((resolve) => {
            const {
                application,
                displayer,
                player,
            } = session;

            application.emitSessionEnd(
                displayer.id,
                player.id,
            )
                .then(() => {
                    resolve();
                });
        });
    }

    askDisplayerToEndSession(session: SessionModel) {
        const {
            displayerService,
        } = this;

        return new Promise<void>((resolve) => {
            const {
                application,
                displayer,
                player,
            } = session;

            displayer.emitSessionEnd(
                player.id,
                application.id,
            )
                .then(() => displayerService.restituteDisplayer(displayer.id))
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    // Nothing
                });
        });
    }

    askPlayerToEndSession(session: SessionModel) {
        const {
            playerService,
        } = this;

        return new Promise<void>((resolve) => {
            const {
                application,
                displayer,
                player,
            } = session;

            player.emitSessionEnd(
                displayer.id,
                application.id,
            )
                .then(() => playerService.deletePlayer(player.id))
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    // Nothing
                });
        });
    }
}
