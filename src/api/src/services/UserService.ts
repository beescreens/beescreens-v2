import debug from 'debug';
import { UserModel } from '../models/UserModel';
import { Mongoose, Model } from 'mongoose';
import { Role } from '../utils/common/Role';
import { Status } from '../utils/common/Status';

const log = debug('beescreens:user-service');

export class UserService {
    model: Model<any>;

    constructor(mongoose: Mongoose) {
        const { Schema } = mongoose;

        const userSchema = new Schema({
            username: {
                type: String,
                unique: true,
                required: true,
                dropDups: true,
            },
            password: {
                type: String,
                required: true,
            },
            role: {
                type: String,
                required: true,
            },
            status: {
                type: String,
                required: true,
            },
        });

        this.model = mongoose.model('User', userSchema);
    }

    addUser(
        username: string,
        password: string,
        role: Role,
    ) {
        const { model } = this;

        return new Promise<void>((resolve, reject) => {
            log(`User '${username}' will be added.`);

            this.getUser(username)
                .then(() => {
                    log(`User with username '${username}' already exists.`);
                    reject();
                })
                .catch(() => {
                    const newUser = new UserModel(
                        username,
                        password,
                        role,
                        Status.ACTIVE,
                    );

                    const record = new model(newUser);

                    record.save()
                        .then(() => {
                            log(`User '${username}' has been successfully added.`);
                            resolve();
                        });
                });
        });
    }

    deleteUser(username: string) {
        const { model } = this;

        return new Promise((resolve, reject) => {
            log(`User '${username}' will be deleted.`);

            this.getUser(username)
                .then(() => {
                    model.findOneAndDelete({ username }, () => {
                        log(`User '${username}' has been successfully deleted.`);
                        resolve();
                    });
                })
                .catch(() => {
                    log(`User '${username}' not found.`);
                    reject();
                });
        });
    }

    getUser(username: string) {
        const { model } = this;

        return new Promise<UserModel>((resolve, reject) => {
            log(`User '${username}' will be retrieved.`);

            model.findOne({ username }, (err: Error, user: UserModel) => {
                if (user != null) {
                    log(`User '${username}' has been successfully retrieved.`);
                    resolve(user);
                } else {
                    log(`User '${username}' not found.`);
                    reject();
                }
            });
        });
    }

    getUsers() {
        return new Promise<Array<UserModel>>((resolve) => {
            const { model } = this;

            model.find({}, (err: any, users: Array<UserModel>) => {
                log(`Users have been successfully retrieved.`);
                resolve(users);
            });
        });
    }

    updateUser(
        username: string,
        password: string,
        role: Role,
        status: Status,
    ) {
        const { model } = this;

        return new Promise<void>((resolve, reject) => {
            log(`User '${username}' will be updated.`);

            model.findOne({ username }, (err, foundUser) => {
                if (err == null) {
                    foundUser.password = password;
                    foundUser.role = role;
                    foundUser.status = status;

                    foundUser.save()
                        .then(() => {
                            log(`User '${username}' has been successfully updated.`);
                            resolve();
                        })
                        .catch(() => {
                            log(`User '${username}' has not been successfully updated.`);
                            reject();
                        });
                } else {
                    log(`User '${username}' has not been successfully updated.`);
                    reject();
                }
            });
        });
    }
}
