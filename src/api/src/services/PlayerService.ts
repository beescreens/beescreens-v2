import debug from 'debug';
import { PlayerModel } from '../models/PlayerModel';
import { Status } from '../utils/common/Status';

const log = debug('beescreens:player-service');

export class PlayerService {
    players: Map<string, PlayerModel>;

    constructor() {
        const players = new Map<string, PlayerModel>();

        this.players = players;
    }

    deletePlayer(playerId: string) {
        const { players } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Player '${playerId}' will be deleted.`);

            this.getPlayer(playerId)
                .then(() => {
                    players.delete(playerId);

                    log(`Player '${playerId}' has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    log(`Player '${playerId}' not found.`);
                    reject();
                });
        });
    }

    getPlayer(playerId: string) {
        const { players } = this;

        return new Promise<PlayerModel>((resolve, reject) => {
            log(`Player '${playerId}' will be retrieved.`);
            const player = players.get(playerId);

            if (player != null) {
                log(`Player '${playerId}' has been successfully retrieved.`);
                resolve(player);
            } else {
                log(`Player '${playerId}' not found.`);
                reject();
            }
        });
    }

    getPlayers() {
        const { players } = this;

        return new Promise<Array<PlayerModel>>((resolve) => {
            log(`Players have been successfully retrieved.`);
            resolve([...players.values()]);
        });
    }

    updatePlayer(
        playerId: string,
        status: Status,
    ) {
        const { players } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Player '${playerId}' will be updated.`);

            this.getPlayer(playerId)
                .then((player) => {
                    player.status = status;

                    players.set(playerId, player);

                    resolve();
                })
                .catch(() => {
                    log(`Player '${playerId}' not found.`);
                    reject();
                });
        });
    }

    registerPlayer(
        id: string,
    ) {
        const { players } = this;

        return new Promise<PlayerModel>((resolve) => {
            const player = new PlayerModel(id, Status.ACTIVE);

            players.set(id, player);

            log(`Player '${id}' has been successfully added.`);
            resolve(player);
        });
    }
}
