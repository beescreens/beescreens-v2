import debug from 'debug';

import { Author } from '../utils/common/Author';
import { ApplicationModel } from '../models/ApplicationModel';
import { Status } from '../utils/common/Status';

const log = debug('beescreens:application-service');

export class ApplicationService {
    applications: Map<string, ApplicationModel>;

    constructor() {
        const applications = new Map<string, ApplicationModel>();

        this.applications = applications;
    }

    deleteApplication(applicationId: string): Promise<void> {
        const { applications } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Application '${applicationId}' will be deleted.`);

            this.getApplication(applicationId)
                .then(() => {
                    applications.delete(applicationId);

                    log(`Application '${applicationId}' has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    log(`Application '${applicationId}' not found.`);
                    reject();
                });
        });
    }

    getApplication(applicationId: string) {
        const { applications } = this;

        return new Promise<ApplicationModel>((resolve, reject) => {
            log(`Application '${applicationId}' will be retrieved.`);

            const application = applications.get(applicationId);

            if (application != null) {
                log(`Application '${applicationId}' has been successfully retrieved.`);
                resolve(application);
            } else {
                log(`Application '${applicationId}' not found.`);
                reject();
            }
        });
    }

    getApplications() {
        const { applications } = this;

        return new Promise<Array<ApplicationModel>>((resolve) => {
            log(`Applications have been successfully retrieved.`);
            resolve([...applications.values()]);
        });
    }

    registerApplication(
        id: string,
        name: string,
        description: string,
        logo: string,
        homepage: string,
        documentation: string,
        version: number,
        authors: Array<Author>,
    ) {
        const {
            applications,
        } = this;

        return new Promise<ApplicationModel>((resolve) => {
            const application = new ApplicationModel(
                id,
                name,
                description,
                logo,
                homepage,
                documentation,
                version,
                authors,
                Status.ACTIVE,
            );

            applications.set(id, application);

            log(`Application '${id}' has been successfully registered.`);
            resolve(application);
        });
    }

    updateApplication(applicationId: string, status: Status) {
        const { applications } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Application '${applicationId}' will be updated.`);

            this.getApplication(applicationId)
                .then((application) => {
                    application.status = status;

                    applications.set(applicationId, application);

                    log(`Application '${applicationId}' has been successfully updated.`);
                    resolve();
                })
                .catch(() => {
                    log(`Application '${applicationId}' not found.`);
                    reject();
                });
        });
    }
}
