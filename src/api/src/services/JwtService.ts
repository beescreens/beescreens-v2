import { sign, verify, VerifyErrors } from 'jsonwebtoken';

import { JwtModel } from '../models/JwtModel';
import { Token } from '../utils/common/Token';

export class JwtService {
    jwtSecret: string;
    jwtExpiration: string;
    knownJwts: Map<string, JwtModel>;
    blacklistedJwts: Map<string, JwtModel>;

    constructor(jwtSecret: string, jwtExpiration: string) {
        this.jwtSecret = jwtSecret;
        this.jwtExpiration = jwtExpiration;

        this.knownJwts = new Map<string, JwtModel>();
        this.blacklistedJwts = new Map<string, JwtModel>();
    }

    sign(payload: Token) {
        const { jwtExpiration, jwtSecret } = this;

        return new Promise<string>((resolve) => {
            const json = JSON.stringify(payload);
            const plainObject = JSON.parse(json);

            const token = sign(plainObject, jwtSecret, {
                expiresIn: jwtExpiration,
            });

            this.addKnownJwt(token);

            resolve(token);
        });
    }

    verify(token: string) {
        const { jwtSecret } = this;

        return new Promise<Token>((resolve, reject) => {
            const isTokenValid = (
                err: VerifyErrors,
                decodedToken: any,
            ) => {
                if (err != null) {
                    reject();
                }

                resolve(decodedToken);
            };

            verify(
                token,
                jwtSecret,
                undefined,
                isTokenValid,
            );
        });
    }

    isJwtKnown(jwt: string) {
        const { knownJwts } = this;

        return new Promise<void>((resolve, reject) => {
            if (knownJwts.has(jwt)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    isJwtBlacklisted(jwt: string) {
        const { blacklistedJwts } = this;

        return new Promise<void>((resolve, reject) => {
            if (blacklistedJwts.has(jwt)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    addKnownJwt(jwt: string) {
        const { knownJwts } = this;

        return new Promise<void>((resolve) => {
            knownJwts.set(jwt, new JwtModel(jwt));
            resolve();
        });
    }

    addBlacklistedJwt(jwt: string) {
        const { blacklistedJwts } = this;

        return new Promise<void>((resolve) => {
            blacklistedJwts.set(jwt, new JwtModel(jwt));
            resolve();
        });
    }

    removeKnownJwt(jwt: string) {
        const { knownJwts } = this;

        return new Promise<void>((resolve) => {
            knownJwts.delete(jwt);
            resolve();
        });
    }

    removeBlacklistedJwt(jwt: string) {
        const { blacklistedJwts } = this;

        return new Promise<void>((resolve) => {
            blacklistedJwts.delete(jwt);
            resolve();
        });
    }
}
