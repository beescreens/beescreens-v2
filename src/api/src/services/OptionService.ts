import debug from 'debug';

import { OptionModel } from '../models/OptionModel';

const log = debug('beescreens:option-service');

export class OptionService {
    options: Map<string, OptionModel>;

    constructor(
        acceptNewApplications: boolean,
        acceptKnownApplications: boolean,
        applicationsPassword: string,

        acceptNewDisplayers: boolean,
        acceptKnownDisplayers: boolean,
        displayersPassword: string,

        acceptPlayers: boolean,
        playersPassword: string,
    ) {
        const options = new Map<string, OptionModel>();

        [
            // Applications options
            new OptionModel(
                'accept_new_applications',
                'Accept new applications',
                'Accept new applications to connect on the server',
                acceptNewApplications,
            ),
            new OptionModel(
                'accept_known_applications',
                'Accept known applications',
                'Accept known applications to connect on the server',
                acceptKnownApplications,
            ),
            new OptionModel(
                'applications_password',
                'Applications password',
                'The password asked to be accepted as a new application (empty means no password is needed)',
                applicationsPassword,
            ),
            // Displayers options
            new OptionModel(
                'accept_new_displayers',
                'Accept new displayers',
                'Accept new displayers to connect on the server',
                acceptNewDisplayers,
            ),
            new OptionModel(
                'accept_known_displayers',
                'Accept known displayers',
                'Accept known displayers to connect on the server',
                acceptKnownDisplayers,
            ),
            new OptionModel(
                'displayers_password',
                'Displayers password',
                'The password asked to be accepted as a new displayer (empty means no password is needed)',
                displayersPassword,
            ),
            // Players options
            new OptionModel(
                'accept_players',
                'Accept players',
                'Accept players to connect on the server',
                acceptPlayers,
            ),
            new OptionModel(
                'players_password',
                'Players password',
                'The password asked to be accepted as a new player (empty means no password is needed)',
                playersPassword,
            ),
        ].forEach((option) => {
            options.set(option.id, option);
        });

        this.options = options;
    }

    changeOptionValue(
        optionId: string,
        value: any,
    ) {
        const { options } = this;

        return new Promise<void>((resolve, reject) => {
            log(`Option value '${optionId}' will be updated.`);

            const option = options.get(optionId);

            if (option != null) {
                option.value = value;

                options.set(optionId, option);

                log(`Option value '${optionId}' has been successfully updated.`);
                resolve();
            } else {
                log(`Option value '${optionId}' not found.`);
                reject();
            }
        });
    }

    getOption(optionId: string) {
        const { options } = this;

        return new Promise<any>((resolve, reject) => {
            log(`Option value '${optionId}' will be retrieved.`);

            const option = options.get(optionId);

            if (option != null) {
                log(`Option value '${optionId}' has been successfully retrieved.`);
                resolve(option);
            } else {
                log(`Option value '${optionId}' not found.`);
                reject();
            }
        });
    }

    getOptions() {
        const { options } = this;

        return new Promise<Array<OptionModel>>((resolve) => {
            log(`Options have been successfully retrieved.`);
            resolve([...options.values()]);
        });
    }
}
