import { IpModel } from '../models/IpModel';

export class IpService {
    knownIps: Map<string, IpModel>;
    blacklistedIps: Map<string, IpModel>;

    constructor() {
        this.knownIps = new Map<string, IpModel>();
        this.blacklistedIps = new Map<string, IpModel>();
    }

    isIpKnown(ip: string) {
        const { knownIps } = this;

        return new Promise<void>((resolve, reject) => {
            if (knownIps.has(ip)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    isIpBlacklisted(ip: string) {
        const { blacklistedIps } = this;

        return new Promise<void>((resolve, reject) => {
            if (blacklistedIps.has(ip)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    addKnownIp(ip: string) {
        const { knownIps } = this;

        return new Promise<void>((resolve) => {
            knownIps.set(ip, new IpModel(ip));
            resolve();
        });
    }

    addBlacklistedIp(ip: string) {
        const { blacklistedIps } = this;

        return new Promise<void>((resolve) => {
            blacklistedIps.set(ip, new IpModel(ip));
            resolve();
        });
    }

    removeKnownIp(ip: string) {
        const { knownIps } = this;

        return new Promise<void>((resolve) => {
            knownIps.delete(ip);
            resolve();
        });
    }

    removeBlacklistedIp(ip: string) {
        const { blacklistedIps } = this;

        return new Promise<void>((resolve) => {
            blacklistedIps.delete(ip);
            resolve();
        });
    }
}
