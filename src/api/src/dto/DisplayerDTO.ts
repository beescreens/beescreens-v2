import { DisplayerModel } from '../models/DisplayerModel';
import { Status } from '../utils/common/Status';

export class DisplayerDTO {
    id: string;
    name: string;
    location: string;
    width: number;
    height: number;
    status: Status;

    constructor(displayer: DisplayerModel) {
        this.id = displayer.id;
        this.name = displayer.name;
        this.location = displayer.location;
        this.width = displayer.width;
        this.height = displayer.height;
        this.status = displayer.status;
    }
}
