import { IpModel } from '../models/IpModel';

export class IpDTO {
    ip: string;
    timestamp: Date;

    constructor(ip: IpModel) {
        this.ip = ip.ip;
        this.timestamp = ip.timestamp;
    }
}
