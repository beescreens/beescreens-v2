import { QueueElementModel } from '../models/QueueElementModel';

export class QueueElementDTO {
    playerId: string;
    applicationId: string;
    position: number;

    constructor(queueElement: QueueElementModel) {
        this.playerId = queueElement.playerId;
        this.applicationId = queueElement.applicationId;
        this.position = queueElement.position;
    }
}
