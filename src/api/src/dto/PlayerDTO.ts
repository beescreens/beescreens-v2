import { PlayerModel } from '../models/PlayerModel';
import { Status } from '../utils/common/Status';

export class PlayerDTO {
    id: string;
    status: Status;

    constructor(player: PlayerModel) {
        this.id = player.id;
        this.status = player.status;
    }
}
