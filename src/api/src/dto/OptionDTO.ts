import { OptionModel } from '../models/OptionModel';

export class OptionDTO {
    id: string;
    name: string;
    description: string;
    value: any;

    constructor(option: OptionModel) {
        this.id = option.id;
        this.name = option.name;
        this.description = option.description;
        this.value = option.value;
    }
}
