import { ApplicationModel } from '../models/ApplicationModel';
import { Author } from '../utils/common/Author';
import { Status } from '../utils/common/Status';

export class ApplicationDTO {
    id: string;
    name: string;
    description: string;
    logo: string;
    homepage: string;
    documentation: string;
    version: number;
    authors: Array<Author>;
    status: Status;

    constructor(application: ApplicationModel) {
        this.id = application.id;
        this.name = application.name;
        this.description = application.description;
        this.logo = application.logo;
        this.homepage = application.homepage;
        this.documentation = application.documentation;
        this.version = application.version;
        this.authors = application.authors;
        this.status = application.status;
    }
}
