import { JwtModel } from '../models/JwtModel';

export class JwtDTO {
    jwt: string;
    timestamp: Date;

    constructor(jwt: JwtModel) {
        this.jwt = jwt.jwt;
        this.timestamp = jwt.timestamp;
    }
}
