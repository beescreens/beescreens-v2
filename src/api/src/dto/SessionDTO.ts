import { SessionModel } from '../models/SessionModel';

export class SessionDTO {
    id: string;
    playerId: string;
    applicationId: string;
    displayerId: string;

    constructor(session: SessionModel) {
        this.id = session.id;
        this.playerId = session.player.id;
        this.applicationId = session.application.id;
        this.displayerId = session.displayer.id;
    }
}
