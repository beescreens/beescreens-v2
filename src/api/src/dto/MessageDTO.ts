import { MessageModel } from '../models/MessageModel';
import { Level } from '../utils/common/Level';

export class MessageDTO {
    title: string;
    content: string;
    level: Level;

    constructor(message: MessageModel) {
        this.title = message.title;
        this.content = message.content;
        this.level = message.level;
    }
}
