import { UserModel } from '../models/UserModel';
import { Role } from '../utils/common/Role';
import { Status } from '../utils/common/Status';

export class UserDTO {
    username: string;
    role: Role;
    status: Status;

    constructor(user: UserModel) {
        this.username = user.username;
        this.role = user.role;
        this.status = user.status;
    }
}
