import debug from 'debug';

import { Response, NextFunction } from 'express';

import { container } from '../container';
import { Response403 } from '../utils/responses/Response403';
import { IpService } from '../services/IpService';

const log = debug('beescreens:ip-helper');

export function verify(
    req: any,
    res: Response,
    next: NextFunction,
) {
    const ipService: IpService = container.cradle.ipService;

    const { ip } = req;

    log(`IP '${ip}' is being verified.`);

    let known = false;
    let blacklisted = false;

    ipService.isIpKnown(ip)
        .then(() => {
            known = true;

            return ipService.isIpBlacklisted(ip);
        })
        .then(() => {
            blacklisted = true;
        })
        .catch(() => {
            // Nothing
        })
        .finally(() => {
            if (blacklisted) {
                log(`IP '${ip}' is blacklisted.`);
                Response403.send(req.res);
            } else {
                if (known) {
                    log(`IP '${ip}' is known`);
                } else {
                    log(`IP '${ip}' is unknown and added to known IPs.`);
                    ipService.addKnownIp(ip);
                }

                next();
            }
        });
}
