import debug from 'debug';

import { container } from '../container';
import { JwtService } from '../services/JwtService';
import { SwaggerSecurityCallback } from 'swagger-tools';
import { Response403 } from '../utils/responses/Response403';

const log = debug('beescreens:jwt-helper');

export function verify(
    req: any,
    authOrSecDef: any,
    bearer: any,
    next: SwaggerSecurityCallback,
) {
    const jwtService: JwtService = container.cradle.jwtService;

    // Get scopes from the request
    const scopes = req.swagger.operation['x-security-scopes'];

    // Check if the Bearer is correctly formated
    if (bearer && bearer.indexOf('Bearer ') === 0) {
        const token = bearer.replace('Bearer ', '');

        log(`Token '${token}' is being verified.`);

        let known = false;
        let blacklisted = false;

        jwtService.isJwtKnown(token)
            .then(() => {
                known = true;

                return jwtService.isJwtBlacklisted(token);
            })
            .then(() => {
                blacklisted = true;
            })
            .catch(() => {
                // Nothing
            })
            .finally(() => {
                if (!known) {
                    log(`Token '${token}' is unknown.`);
                    Response403.send(req.res);
                } else if (blacklisted) {
                    log(`Token '${token}' is blacklisted.`);
                    Response403.send(req.res);
                } else {
                    jwtService
                        .verify(token)
                        .then((decodedToken) => {
                            // Check if the scopes are correctly formated and decoded token has role
                            if (Array.isArray(scopes) && decodedToken.role) {
                                const rolesMatch = scopes.indexOf(decodedToken.role) !== -1;

                                if (rolesMatch) {
                                    log(`Token '${token}': roles match.`);
                                    req.auth = token;
                                    next();
                                } else {
                                    log(`Token '${token}': roles do not match.`);
                                    Response403.send(req.res);
                                }
                            }
                        })
                        .catch(() => {
                            log(`Token '${token}' is invalid.`);
                            Response403.send(req.res);
                        });
                }
            });
    } else {
        log('No valid Bearer was provided.');
        Response403.send(req.res);
    }
}
