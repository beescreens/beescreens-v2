// import compression from 'compression';
import Cors from 'cors';
import debug from 'debug';
import Express from 'express';
import Helmet from 'helmet';
import Http from 'http';
import Path from 'path';
import Swagger from 'swagger-tools';

import { verify as ipHelper } from './helpers/IpHelper';
import { verify as jwtHelper } from './helpers/JwtHelper';

const log = debug('beescreens:rest-server');

export class RestServer {
    express: Express.Express;
    instance: Http.Server;

    /**
     * The constructor.
     */
    constructor(
        public apiHostname: string,
        public apiPort: number,
        swaggerSpec: object,
    ) {
        // Express
        const express = Express();

        // Swagger
        Swagger.initializeMiddleware(swaggerSpec, (middleware) => {
            express.use(
                // Check if IP is not blacklisted
                ipHelper,

                // Add CORS headers
                Cors(),

                // Remove unsecure headers
                Helmet(),

                // errorHandler,

                // Interpret Swagger resources and attach metadata to request
                middleware.swaggerMetadata(),

                // Verify the token
                middleware.swaggerSecurity({
                    Bearer: jwtHelper,
                }),

                // Validate Swagger requests
                middleware.swaggerValidator(),

                // Route validated requests to appropriate controller
                middleware.swaggerRouter({
                    controllers: Path.resolve(__dirname, './controllers'),
                }),

                // Serve the Swagger documents and Swagger UI
                middleware.swaggerUi({
                    swaggerUi: '/',
                }),

                // Compress requests
                // compression,
            );
        });

        const instance = Http.createServer(express);

        this.express = express;
        this.instance = instance;
    }

    /**
     * Start the server.
     */
    start() {
        const {
            apiHostname,
            apiPort,
            instance,
        } = this;

        return new Promise<void>((resolve) => {
            instance.listen(apiPort, () => {
                log(`Listening on ${apiHostname}:${apiPort}`);
                resolve();
            });
        });
    }

    /**
     * Stop the server.
     */
    stop() {
        const {
            instance,
        } = this;

        return new Promise<void>((resolve) => {
            instance.close(() => {
                log('Server gracefully stopped.');
                resolve();
            });
        });
    }
}
