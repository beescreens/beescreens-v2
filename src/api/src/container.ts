const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

import * as awilix from 'awilix';
import fs from 'fs';
import jsyaml from 'js-yaml';
import path from 'path';

import { Database, connection, mongoose } from './Database';
import { IoServer } from './IoServer';
import { PeerServer } from './PeerServer';
import { RestServer } from './RestServer';

import { ApplicationService } from './services/ApplicationService';
import { DisplayerService } from './services/DisplayerService';
import { IpService } from './services/IpService';
import { JwtService } from './services/JwtService';
import { LoginLogoutService } from './services/LoginLogoutService';
import { MessageService } from './services/MessageService';
import { OptionService } from './services/OptionService';
import { PlayerService } from './services/PlayerService';
import { PlayService } from './services/PlayService';
import { QueueService } from './services/QueueService';
import { SessionService } from './services/SessionService';
import { UserService } from './services/UserService';

const {
    asClass, asValue, createContainer, InjectionMode,
} = awilix;

const container = createContainer({
    injectionMode: InjectionMode.PROXY,
});

// Register parameters
container.register({
    apiHostname: asValue(process.env.API_HOSTNAME),
    apiPort: asValue(process.env.API_PORT),

    adminUsername: asValue(process.env.ADMIN_USERNAME),
    adminPassword: asValue(process.env.ADMIN_PASSWORD),

    jwtSecret: asValue(process.env.JWT_SECRET),
    jwtExpiration: asValue(process.env.JWT_EXPIRATION),

    acceptKnownApplications: asValue(process.env.ACCEPT_NEW_APPLICATIONS),
    acceptNewApplications: asValue(process.env.ACCEPT_KNOWN_APPLICATIONS),
    applicationsPassword: asValue(process.env.APPLICATIONS_PASSWORD),

    acceptKnownDisplayers: asValue(process.env.ACCEPT_NEW_DISPLAYERS),
    acceptNewDisplayers: asValue(process.env.ACCEPT_KNOWN_DISPLAYERS),
    displayersPassword: asValue(process.env.DISPLAYERS_PASSWORD),

    acceptPlayers: asValue(process.env.ACCEPT_PLAYERS),
    playersPassword: asValue(process.env.PLAYERS_PASSWORD),

    dbUsername: asValue(process.env.DB_USERNAME),
    dbPassword: asValue(process.env.DB_PASSWORD),
    dbHostname: asValue(process.env.DB_HOSTNAME),
    dbPort: asValue(process.env.DB_PORT),
    dbName: asValue(process.env.DB_NAME),

    swaggerSpec: asValue(
        jsyaml.safeLoad(
            fs.readFileSync(
                path.resolve(__dirname, './swagger/api.yml'), 'utf8',
            ),
        ),
    ),
});

// Register main classes
container.register({
    database: asClass(Database).classic().singleton(),
    ioServer: asClass(IoServer).classic().singleton(),
    peerServer: asClass(PeerServer).classic().singleton(),
    restServer: asClass(RestServer).classic().singleton(),
    mongooseConnection: asValue(connection),
    mongoose: asValue(mongoose),

    applicationService: asClass(ApplicationService).classic().singleton(),
    displayerService: asClass(DisplayerService).classic().singleton(),
    ipService: asClass(IpService).classic().singleton(),
    jwtService: asClass(JwtService).classic().singleton(),
    loginLogoutService: asClass(LoginLogoutService).classic().singleton(),
    messageService: asClass(MessageService).classic().singleton(),
    optionService: asClass(OptionService).classic().singleton(),
    playerService: asClass(PlayerService).classic().singleton(),
    playService: asClass(PlayService).classic().singleton(),
    queueService: asClass(QueueService).classic().singleton(),
    sessionService: asClass(SessionService).classic().singleton(),
    userService: asClass(UserService).classic().singleton(),
});

// Export container
export {
    container,
};
