# BeeScreens - `src/api/`

This part of the application depends on:

- [`src/db/`](https://gitlab.com/beescreens/beescreens/tree/master/src/db)

And can be tested with:

- [`test/api/`](https://gitlab.com/beescreens/beescreens/tree/master/test/api)

## Launch

### Locally

```sh
# Move to the cloned directory
cd beescreens/src/api/

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the API
npm run serve
```

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
cp .env.dist .env

# Start the API
docker-compose up --build api
```


