# BeeScreens - `src/apps/`

This directory contains all the applications developped for BeeScreens.

Each directory contains the specific third-party application documentation.

## Third-party app development

### Getting started
TODO

## Third-party apps

- [drawing-app](https://gitlab.com/beescreens/beescreens/tree/master/src/apps/drawing-app): Draw and paint whatever you want !