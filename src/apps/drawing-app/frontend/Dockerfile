# Build stage
FROM node:latest as build-stage

# Args
ARG NODE_ENV
ARG DEBUG
ARG VUE_APP_PROTOCOL
ARG VUE_APP_API_HOSTNAME
ARG VUE_APP_API_PORT
ARG VUE_APP_PLAY_HOSTNAME
ARG VUE_APP_PLAY_PORT

# Working directory
WORKDIR /app

# Copy dependencies packages
COPY package*.json /app/

# Install dependencies
RUN npm install inherits
RUN npm install --only=dev && \
    npm install --only=prod

# Copy all sources
COPY . .

# Build the application
RUN npm run lint && \
    npm run build

# Production stage
FROM nginx:stable-alpine as production-stage

# Copy configuration
COPY conf/nginx.conf /etc/nginx/conf.d/default.conf

# Copy sources
COPY --from=build-stage /app/dist /usr/share/nginx/html

# Expose port
EXPOSE 80

# Start the server
CMD ["nginx", "-g", "daemon off;"]