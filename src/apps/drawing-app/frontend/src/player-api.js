import io from 'socket.io-client';

class PlayerApi {
    constructor() {
        this.socket = null;
        this.connected = false;

        this.events = new Map();
    }

    connect(url, jwt) {
        return new Promise((resolve, reject) => {
            this.socket = io.connect(url);

            this.socket.on('connect', () => {
                this.socket.emit('authentication', { jwt });

                this.socket.on('authenticated', () => {
                    this.connected = true;

                    resolve();
                });

                this.socket.on('unauthorized', () => {
                    this.disconnect();

                    reject();
                });

                this.events.forEach((value, key) => {
                    this.socket.on(key, value);
                });
            });
        });
    }

    onDisconnect(fn) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('disconnect', fn);
        } else {
            events.set('disconnect', fn);
        }
    }

    onMessage(fn) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('message', fn);
        } else {
            events.set('message', fn);
        }
    }

    onSessionReady(fn) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('session-ready', fn);
        } else {
            events.set('session-ready', fn);
        }
    }

    onSessionGo(fn) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('session-go', fn);
        } else {
            events.set('session-go', fn);
        }
    }

    onSessionEnd(fn) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('session-end', fn);
        } else {
            events.set('session-end', fn);
        }
    }

    onQueue(fn) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            this.socket.on('queue', fn);
        } else {
            events.set('queue', fn);
        }
    }

    emitPlayerReady() {
        const {
            socket,
        } = this;

        if (socket != null) {
            socket.emit('player-ready');
        }
    }

    emitPlayerData(data) {
        const {
            socket,
        } = this;

        if (socket != null) {
            socket.emit('player-data', data);
        }
    }

    /*
    disconnect() {
        if (this.connected) {
            this.socket.close();
            this.socket = null;
        }

        this.connected = false;
    }
    */
}

export default new PlayerApi();
