import BootstrapVue from 'bootstrap-vue';
import Toasted from 'vue-toasted';
import Vue from 'vue';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { Chrome } from 'vue-color';

import App from './App.vue';
import playerApi from './player-api';
import router from './router';
import store from './store';

import './assets/scss/main.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

library.add(faHome);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('chrome-picker', Chrome);

Vue.use(BootstrapVue);

Vue.use(Toasted, {
    position: 'center-center',
    duration: 4000,
});

Vue.prototype.playerApi = playerApi;

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
