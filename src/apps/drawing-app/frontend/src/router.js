import Vue from 'vue';
import Router from 'vue-router';

import store from './store';

import Drawing from './views/Drawing.vue';
import Queue from './views/Queue.vue';

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'queue',
        component: Queue,
    },
    {
        path: '/drawing',
        name: 'drawing',
        component: Drawing,
    },
];

const router = new Router({
    mode: 'history',
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.name !== 'queue' && store.getters.jwt == null) {
        next({ name: 'queue' });
    } else {
        next();
    }
});

export default router;
