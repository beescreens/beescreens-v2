/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        jwt: null,
        displayerWidth: null,
        displayerHeight: null,
        application: {
            name: 'Application de dessin',
            logo: 'logo.jpg',
        },
        color: 'black',
        background: 'white',
        size: 7,
    },
    getters: {
        jwt(state) {
            return state.jwt;
        },
        configure() {
            return {
                size: window.innerWidth > window.innerHeight
                    ? window.innerHeight
                    : window.innerWidth,
            };
        },
    },
    mutations: {
        setDisplayerDimensions(state, data) {
            const {
                displayerWidth,
                displayerHeight,
            } = data;

            state.displayerWidth = displayerWidth;
            state.displayerHeight = displayerHeight;
        },
        setJwt(state, jwt) {
            state.jwt = jwt;
        },
        color(state, color) {
            state.color = color;
        },
        size(state, size) {
            state.size = size;
        },
        background(state, background) {
            state.background = background;
        },
    },
});
