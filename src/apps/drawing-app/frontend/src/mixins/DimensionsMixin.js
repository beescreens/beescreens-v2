export default {
    methods: {
        getSize() {
            return window.innerWidth > window.innerHeight
                ? window.innerHeight
                : window.innerWidth;
        },
    },
};
