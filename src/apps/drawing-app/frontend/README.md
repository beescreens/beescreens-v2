# BeeScreens - `src/apps/drawing-app/frontend/`

This part of the application depends on:

- [`src/apps/drawing-app/backend/`](https://gitlab.com/beescreens/beescreens/tree/master/apps/drawing-app/backend)

And can be tested with:

- [`test/apps/drawing-app/frontend/`](https://gitlab.com/beescreens/beescreens/tree/master/test/apps/drawing-app/frontend)

## Launch

### Locally

```sh
# Move to the cloned directory
cd beescreens/src/apps/drawing-app/frontend/

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the frontend
npm run serve
```

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
cp .env.dist .env

# Start the frontend
docker-compose up --build drawing-app-frontend
```
