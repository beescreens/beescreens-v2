# BeeScreens - `src/apps/drawing-app/`

This application allow to draw in real-time.

Have a look at the two sub-parts of this application for further documentation:

- [`src/apps/drawing-app/frontend/`](https://gitlab.com/beescreens/beescreens/tree/master/apps/drawing-app/frontend)
- [`src/apps/drawing-app/backend/`](https://gitlab.com/beescreens/beescreens/tree/master/apps/drawing-app/backend)
