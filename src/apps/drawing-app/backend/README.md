# BeeScreens - `src/apps/drawing-app/backend/`

This part of the application depends on:

- [`src/api/`](https://gitlab.com/beescreens/beescreens/tree/master/src/api)
- [`src/display/`](https://gitlab.com/beescreens/beescreens/tree/master/src/display)
- [`src/play/`](https://gitlab.com/beescreens/beescreens/tree/master/src/play)

And can be tested with:

- [`test/apps/drawing-app/backend/`](https://gitlab.com/beescreens/beescreens/tree/master/test/apps/drawing-app/backend)

## Launch

### Locally

```sh
# Move to the cloned directory
cd beescreens/src/apps/drawing-app/backend/

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the backend
npm run serve
```

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
cp .env.dist .env

# Start the backend
docker-compose up --build drawing-app-backend
```


