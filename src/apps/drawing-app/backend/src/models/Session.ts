import { debug } from 'debug';

import { PageWrapper } from './PageWrapper';

const log = debug('drawing-app:session');

export class Session {
    color : string;
    size : number;
    drawing : boolean;

    constructor(
        public playerId : string,
        public pageWrapper : PageWrapper,
    ) {
        this.color = '#000000';
        this.size = 3;
        this.drawing = false;
    }

    close() {
        const {
            pageWrapper,
        } = this;

        return pageWrapper.close();
    }

    handle(
        name : string,
        payload : any,
    ) {
        const {
            color,
            size,
        } = this;

        return new Promise<void>((resolve, reject) => {
            switch (name) {
            case 'point': {
                const { x, y } = payload;

                log(`drawing [${x}-${y}`);

                this.pageWrapper.drawLine(
                    x,
                    y,
                    color,
                    size,
                )
                    .then(() => {
                        resolve();
                    });
                break;
            }
            case 'mouse': {
                const {
                    action,
                    x,
                    y,
                } = payload;

                if (action === 'down') {
                    this.down(x, y)
                        .then(() => {
                            resolve();
                        });
                } else {
                    this.up(x, y)
                        .then(() => {
                            resolve();
                        });
                }

                break;
            }
            case 'size': {
                this.size = payload.size;
                break;
            }
            case 'color': {
                this.color = payload.color;
                break;
            }
            case 'background': {
                const { background } = payload;
                this.pageWrapper.setBackground(background)
                    .then(() => {
                        resolve();
                    });
                break;
            }
            case 'clear': {
                this.pageWrapper.clearCanvas()
                    .then(() => {
                        resolve();
                    });
                break;
            }
            case 'remove-last': {
                this.pageWrapper.removeLast()
                    .then(() => {
                        resolve();
                    });
                break;
            }
            default: {
                reject();
                break;
            }
            }
        });
    }

    down(
        x : number,
        y : number,
    ) {
        const {
            color,
            drawing,
            size,
        } = this;

        return new Promise((resolve) => {
            if (drawing) {
                this.up(x, y);
            }

            this.drawing = true;

            Promise.all([
                this.pageWrapper.newFabricGroup(),
                this.pageWrapper.drawCircle(x, y, color, size),
            ])
                .then(() => {
                    resolve();
                });
        });
    }

    up(
        x : number,
        y : number,
    ) {
        const {
            color,
            drawing,
            size,
        } = this;

        return new Promise((resolve) => {
            if (!drawing) {
                this.down(x, y);
            }

            this.drawing = false;

            this.pageWrapper.drawCircle(x, y, color, size)
                .then(() => {
                    resolve();
                });
        });
    }
}
