// import debug from 'debug';
import Puppeteer from 'puppeteer';

declare var createCanvas: any;

// const log = debug('drawing-app:page-wrapper');

export class PageWrapper {
    constructor(
        public page : Puppeteer.Page,
    ) {
        // Empty
    }

    init(
        width : number,
        height : number,
        peerServerHostname : string,
        peerServerPort : number,
        displayerId : string,
    ) {
        const {
            page,
        } = this;

        return new Promise<PageWrapper>((resolve) => {
            page.evaluate((canvasWidth, canvasHeight, hostname, port) => {
                createCanvas(
                    canvasWidth,
                    canvasHeight,
                    hostname,
                    port,
                );
            }, width, height, peerServerHostname, peerServerPort)
                .then(() => page.evaluate((displayerKey) => {
                    let canvas : any;

                    canvas = document.getElementById('canvas');

                    canvas.handler.stream(displayerKey);
                }, displayerId))
                .then(() => {
                    resolve(this);
                });
        });
    }

    close() {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.close();
            resolve();
        });
    }

    drawCircle(
            x : number,
            y : number,
            color : string,
            size : number,
        ) {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.evaluate((gx, gy, gcolor, gsize) => {
                let canvas : any;

                canvas = document.getElementById('canvas');

                canvas.handler.drawCircle(gx, gy, gcolor, gsize);
            },
                x,
                y,
                color,
                size)
                    .then(() => {
                        resolve();
                    });
        });
    }

    newFabricGroup() {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.evaluate(() => {
                let canvas : any;

                canvas = document.getElementById('canvas');

                canvas.handler.newGroup();
            })
                    .then(() => {
                        resolve();
                    });
        });
    }

    clearCanvas() {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.evaluate(() => {
                let canvas : any;

                canvas = document.getElementById('canvas');

                canvas.handler.clear();
            })
                    .then(() => {
                        resolve();
                    });
        });
    }

    removeLast() {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.evaluate((canvasId) => {
                let canvas : any;

                canvas = document.getElementById('canvas');

                canvas.handler.removeLast();
            })
                    .then(() => {
                        resolve();
                    });
        });
    }

    setBackground(gcolor : string) {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.evaluate((color) => {
                let canvas : any;

                canvas = document.getElementById('canvas');

                canvas.handler.setBackground(color);
            }, gcolor)
                    .then(() => {
                        resolve();
                    });
        });
    }

    drawLine(
            gx : number,
            gy : number,
            gcolor : string,
            gsize : number,
        ) {
        const {
                page,
            } = this;

        return new Promise((resolve) => {
            page.evaluate((x, y, color, size) => {
                let canvas : any;

                canvas = document.getElementById('canvas');

                canvas.handler.drawLine(x, y, color, size);
            },
                gx,
                gy,
                gcolor,
                gsize)
                    .then(() => {
                        resolve();
                    });
        });
    }
}
