import path from 'path';
import Puppeteer from 'puppeteer';

import { PageWrapper } from '../models/PageWrapper';

export class Renderer {
    browser : Puppeteer.Browser;
    pageWrappers : Array<PageWrapper>;

    constructor() {
        this.browser = null;
        this.pageWrappers = new Array<PageWrapper>();
    }

    init() {
        return new Promise<void>((resolve) => {
            Puppeteer.launch({
                headless: JSON.parse(process.env.DRAWING_APP_HEADLESS),
                args: ['--no-sandbox', '--disable-setuid-sandbox'],
            })
                .then((browser) => {
                    this.browser = browser;
                    resolve();
                });
        });
    }

    newPage(
        width : number,
        height : number,
        beeScreensHostname : string,
        beeScreensPort : number,
        displayerId : string,
    ) {
        const {
            browser,
            pageWrappers,
        } = this;

        return new Promise<PageWrapper>((resolve) => {
            browser.newPage()
                .then((page) => {
                    page.goto(`file:${path.join(__dirname, '../public/index.html')}`)
                        .then(() => {
                            const pageWrapper = new PageWrapper(page);

                            return pageWrapper.init(
                                width,
                                height,
                                beeScreensHostname,
                                beeScreensPort,
                                displayerId,
                            );
                        })
                        .then((pageWrapper) => {
                            pageWrappers.push(pageWrapper);

                            resolve(pageWrapper);
                        });
                });
        });
    }
}
