import debug from 'debug';

import { applicationApi } from '../helpers/ApplicationApi';
import { Session } from '../models/Session';
import { PageWrapper } from '../models/PageWrapper';

const log = debug('drawing-app:session-service');

export class SessionManager {
    sessions : Map<string, Session>;

    constructor(
        public sessionTime: number,
    ) {
        this.sessions = new Map<string, Session>();
    }

    addSession(
        playerId : string,
        pageWrapper : PageWrapper,
    ) {
        const {
            sessions,
            sessionTime,
        } = this;

        return new Promise<Session>((resolve) => {
            const session = new Session(
                playerId,
                pageWrapper,
            );

            const autoDestruction = setTimeout(() => {
                applicationApi.emitSessionEnd();

                log(`Session for player '${playerId}' will be destroyed.`);

                clearTimeout(autoDestruction);
            }, sessionTime * 1000);

            sessions.set(playerId, session);

            log(`Session created for player '${playerId}'`);

            resolve(session);
        });
    }

    deleteSession(playerId : string) {
        const {
            sessions,
        } = this;

        return new Promise((resolve, reject) => {
            log(`Session for player '${playerId}' will be deleted.`);
            this.getSession(playerId)
                .then(() => {
                    sessions.delete(playerId);

                    log(`Session for player '${playerId}' has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    log(`Session for player '${playerId}' not found.`);
                    reject();
                });
        });
    }

    getSession(playerId : string) {
        const {
            sessions,
        } = this;

        return new Promise<Session>((resolve, reject) => {
            log(`Session for player '${playerId}' will be retreived.`);

            const session = sessions.get(playerId);

            if (session != null) {
                log(`Session for player '${playerId}' has been successfully retreived.`);
                resolve(session);
            } else {
                log(`Session for player '${playerId}' not found.`);
                reject();
            }
        });
    }
}
