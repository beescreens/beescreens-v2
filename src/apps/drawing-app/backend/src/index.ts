const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

import Axios from 'axios';
import debug from 'debug';

import { applicationApi } from './helpers/ApplicationApi';
import { Renderer } from './services/RendererService';
import { SessionManager } from './services/SessionService';

const log = debug('drawing-app:index');

const PROTOCOL = process.env.PROTOCOL;
const API_HOSTNAME = process.env.API_HOSTNAME;
const API_PORT = JSON.parse(process.env.API_PORT);

const APPLICATIONS_PASSWORD = process.env.APPLICATIONS_PASSWORD;

const DRAWING_APP_HOSTNAME = process.env.DRAWING_APP_HOSTNAME;
const DRAWING_APP_PORT = JSON.parse(process.env.DRAWING_APP_PORT);
const DRAWING_APP_SESSION_TIME = JSON.parse(process.env.DRAWING_APP_SESSION_TIME);

const client = Axios.create({
    baseURL: `${PROTOCOL}//${API_HOSTNAME}:${API_PORT}`,
    timeout: 2000,
});

const renderer = new Renderer();
const sessionManager = new SessionManager(DRAWING_APP_SESSION_TIME);

renderer.init()
    .then(() => client.post('/apps', {
        id: 'drawing-app',
        name: 'Application de dessin',
        description: 'Dessine sur les écrans géants à la force de tes doigts ! Temps de dessin : 3 minutes.',
        logo: `${PROTOCOL}//${DRAWING_APP_HOSTNAME}:${DRAWING_APP_PORT}/logo.jpg`,
        homepage: `${PROTOCOL}//${DRAWING_APP_HOSTNAME}:${DRAWING_APP_PORT}`,
        documentation: 'https://gitlab.com/beescreens',
        version: 0.1,
        authors: [
            {
                name: 'BeeScreens contributors',
                email: '',
                homepage: 'https://gitlab.com/beescreens',
            },
        ],
        password: APPLICATIONS_PASSWORD,
    }))
    .then(res => res)
    .then((body) => {
        log('ok');
        const {
            jwt,
        } = body.data;

        return Promise.all([
            applicationApi.onSessionReady((payload : any) => {
                const {
                    displayerId,
                    displayerWidth,
                    displayerHeight,
                    playerId,
                } = payload;

                log(`Received 'session-ready' for player '${playerId}'`);

                renderer.newPage(
                    displayerWidth,
                    displayerHeight,
                    API_HOSTNAME,
                    API_PORT,
                    displayerId,
                )
                    .then((pageWrapper) => {
                        return sessionManager.addSession(
                            playerId,
                            pageWrapper,
                        );
                    })
                    .then(() => {
                        log(`Session for player '${playerId}' `
                            + `is ready. Sending 'application-ready'.`);
                        applicationApi.emitApplicationReady();
                    });
            }),
            applicationApi.onPlayerData((payload : any) => {
                const {
                    playerId,
                    data,
                } = payload;

                log(`Player '${playerId}' is sending data to application.`);

                sessionManager.getSession(playerId)
                    .then((session) => {
                        session.handle(data.name, data.payload);
                    });
            }),
            applicationApi.onSessionEnd((payload : any) => {
                const {
                    playerId,
                } = payload;

                log(`Received 'session-end' for player '${playerId}'`);

                sessionManager.getSession(playerId)
                    .then(session => session.close())
                    .then(() => sessionManager.deleteSession(playerId))
                    .then(() => {
                        log(`Session ended for player '${playerId}'`);
                    })
                    .catch(() => {
                        // Nothing
                    });
            }),
            applicationApi.connect(
                `${PROTOCOL}//${API_HOSTNAME}:${API_PORT}/applications`,
                jwt,
            ),
        ]);
    })
    .then(() => {
        log('Connected.');
    })
    .catch((err) => {
        log('Cannot connect now.');
        log(err);
    });
