class CanvasHandler {
    constructor(
        canvas,
        peerServerHostname,
        peerServerPort,
    ) {
        this.canvas = canvas;
        this.peerServerHostname = peerServerHostname;
        this.peerServerPort = peerServerPort;
        this.coords = null;
    }

    stream(displayerId) {
        try {
            const id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

            this.peer = new Peer(id, {
                host: this.peerServerHostname,
                port: this.peerServerPort,
                path: '/peers',
                config: {
                    iceServers: [
                        { url: 'stun:stun01.sipphone.com' },
                        { url: 'stun:stun.ekiga.net' },
                        { url: 'stun:stunserver.org' },
                        { url: 'stun:stun.softjoys.com' },
                        { url: 'stun:stun.voiparound.com' },
                        { url: 'stun:stun.voipbuster.com' },
                        { url: 'stun:stun.voipstunt.com' },
                        { url: 'stun:stun.voxgratia.org' },
                        { url: 'stun:stun.xten.com' },
                    ],
                },
            });

            console.log(`Connecting to peer server with id ${id}`);

            this.peer.on('open', () => {
                const stream = this.canvas.captureStream(30);

                console.log(`Calling displayer ${displayerId} with stream`);

                this.peer.call(displayerId, stream);
            });
        } catch (e) {
            console.log(`Failed calling displayer ${displayerId}`);
        }
    }

    clear() {
        this.canvas.groups.forEach((group) => {
            group.forEach((line) => {
                this.canvas.fabric.remove(line);
            });
        });
        this.canvas.groups.clear();
    }

    removeLast() {
        const group = this.canvas.groups.pop();

        group.forEach((line) => {
            this.canvas.fabric.remove(line);
        });
    }

    remove() {
        if (this.canvas.peer) {
            this.canvas.peer.destroy();
        }

        if (this.canvas.fabric) {
            this.canvas.fabric.dispose();
        }
    }

    newGroup() {
        this.coords = null;
        this.canvas.groups.add();
    }

    drawLine(x, y, color, size) {
        if (this.coords === null) {
            this.coords = { x, y };
            return;
        }

        const oldCoords = this.coords;
        this.coords = { x, y };
        
        const line = new fabric.Line([oldCoords.x, oldCoords.y, x, y], {
            fill: color,
            stroke: color,
            strokeWidth: size,
        });

        this.canvas.groups.current().push(line);
        this.canvas.fabric.add(line);
    }

    drawCircle(x, y, color, size) {
        if (this.coords !== null && this.coords.x === y && this.coords.y === y) return;
        this.coords = { x, y };
        
        const circle = new fabric.Circle({
            originX: 'center',
            originY: 'center',
            left: x,
            top: y,
            radius: size / 2,
            fill: color,
        });
        this.canvas.groups.current().push(circle);
        this.canvas.fabric.add(circle);
    }

    setBackground(color) {
        this.canvas.fabric.setBackgroundColor(color);
        this.canvas.fabric.renderAll();
    }
}

class GroupHandler {
    constructor() {
        this.groups = [];
    }

    add() {
        const group = [];
        this.groups.push(group);
        return group;
    }

    current() {
        return this.groups.length === 0
            ? this.add()
            : this.groups[this.groups.length - 1];
    }

    pop() {
        return this.groups.pop();
    }

    clear() {
        this.groups.splice(0, this.groups.length);
    }

    forEach(fn) {
        this.groups.forEach((group) => {
            fn(group);
        });
    }
}

function createCanvas(width, height, peerServerHostname, peerServerPort) {
    const canvas = document.querySelector('canvas');
    canvas.peer = null;
    
    canvas.fabric = new fabric.Canvas(canvas, {
        width,
        height,
        backgroundColor: 'white',
    });
    canvas.groups = new GroupHandler();
    canvas.handler = new CanvasHandler(canvas, peerServerHostname, peerServerPort);
}
