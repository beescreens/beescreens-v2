import io from 'socket.io-client';

class ApplicationApi {
    socket: SocketIOClient.Socket;
    connected: boolean;
    events: Map<string, Function>;

    constructor() {
        this.socket = null;
        this.connected = false;

        this.events = new Map<string, Function>();
    }

    connect(
        url: string,
        jwt: string,
    ) {
        return new Promise<void>((resolve, reject) => {
            this.socket = io.connect(url);

            this.socket.on('connect', () => {
                this.socket.emit('authentication', { jwt });

                this.socket.on('authenticated', () => {
                    this.connected = true;

                    resolve();
                });

                this.socket.on('unauthorized', () => {
                    this.disconnect();

                    reject();
                });

                this.events.forEach((value, key) => {
                    this.socket.on(key, value);
                });
            });
        });
    }

    onDisconnect(
        fn: Function,
    ) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('disconnect', fn);
        } else {
            events.set('disconnect', fn);
        }
    }

    onMessage(
        fn: Function,
    ) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('message', fn);
        } else {
            events.set('message', fn);
        }
    }

    onSessionReady(
        fn: Function,
    ) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('session-ready', fn);
        } else {
            events.set('session-ready', fn);
        }
    }

    onSessionGo(
        fn: Function,
    ) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('session-go', fn);
        } else {
            events.set('session-go', fn);
        }
    }

    onSessionEnd(
        fn: Function,
    ) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            socket.on('session-end', fn);
        } else {
            events.set('session-end', fn);
        }
    }

    onPlayerData(
        fn: Function,
    ) {
        const {
            events,
            socket,
        } = this;

        if (socket != null) {
            this.socket.on('player-data', fn);
        } else {
            events.set('player-data', fn);
        }
    }

    emitApplicationReady() {
        const {
            socket,
        } = this;

        if (socket != null) {
            socket.emit('application-ready');
        }
    }

    emitApplicationData(data: Object) {
        const {
            socket,
        } = this;

        if (socket != null) {
            socket.emit('application-data', data);
        }
    }

    emitSessionEnd() {
        const {
            socket,
        } = this;

        if (socket != null) {
            socket.emit('session-end');
        }
    }

    disconnect() {
        if (this.connected) {
            this.socket.close();
            this.socket = null;
        }

        this.connected = false;
    }
}

export const applicationApi = new ApplicationApi();
