# BeeScreens - `src/db/`

This part of the application depends on:

- Nothing

## Launch

### Docker

```sh
# Move to the cloned directory
cd beescreens

# Copy and edit the environment variables
cp .env.dist .env

# Start the UI
docker-compose up --build db
```
